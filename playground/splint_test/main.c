
int getSize(/*@untainted@*/char* s) {
	return sizeof(s);
}

int main() {
	/*@tainted@*/ char *s = malloc (sizeof (char) * 3);
	char *t = malloc (sizeof (char) * strlen (s));
	
	(void)system(t); //ok
	(void)system(s); //error
	
	strcpy (t, s);
	(void)system(t); //error
}
