#include <stdio.h>
#include <stdlib.h>

char * getUserData() {
	return getenv("HOME");
}

char * testTaintPropagation(char * data) {
	char * res = data + 1;
	if (sizeof(res) > 0) {
		return 0;
	} else {
		return res;
	}
}

char * untaint(char * data) {
	char * res = (char $untainted *) data;
	return res;
}

int main() {
	// tainting
	char * userData = getUserData();
	
	// taint propagation
	// expression
  char * data2 = userData + 3;
  
  //function
  char * data3 = testTaintPropagation(data2);
  
  //data3 should be tainted
  printf(data3); // Should be Error.
  
  //untaint data3
 	char * data4 = untaint(data3);
 
 	printf(data4); // Should be OK
 	
  return 0;
}
