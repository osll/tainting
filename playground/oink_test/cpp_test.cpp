int $untainted untaining(int i, int $untainted u) {
	if (i < 0) return u;
	 else return i;
}

template<class T> struct A {
  virtual T f(int x) {}
};

struct B : A<int> {
  int &y;
  B(int &y0) : y(y0) {}
  int f(int x) {
    y = x;
  }
};

int main() {
  int $untainted y;
  A<int> *a0 = new B(y);
  int (A<int>::*q)(int) = &A<int>::f;
  int $tainted x = 0;
  int $untainted z = x + 10 + y;
  (a0->*q)(z);
}
