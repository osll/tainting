import checkers.tainting.quals.*;

public class TaintTest {

    public static void main(String[] args) {
        String taintString = args[0];
        String goodString = "Good String";

        //makeSomeDangerous(taintString);	//Compile Error

        makeSomeDangerous(validate(taintString)); // OK

        makeSomeDangerous(goodString); // OK

        //makeSomeDangerous("Just string, concatenated with taint string" + taintString); //Compile Error
    }

    private static void makeSomeDangerous(@Untainted String query) {
        //do some dangerous
    }

    @SuppressWarnings("untainted")
    private static /*@Untainted*/String validate(String userInput) {
        for (int i = 0; i < userInput.length(); ++i) {
            char ch = userInput.charAt(i);
            if (!Character.isLetter(ch)
                    && !Character.isDigit(ch)
                    && !Character.isWhitespace(ch))
                throw new IllegalArgumentException("Illegal user input");
        }
        return (/*@Untainted*/ String) userInput;
    }
}
