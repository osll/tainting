#!/usr/bin/perl -T
    
use Scalar::Util qw(tainted);

sub func ($){
	my $arg = $_[0];
	return $arg . " hello"; 
}

my $arg = $ARGV[0];
print "arg -- ".(tainted($arg)?"is ":"is not ")."tainted.\n";

$arg = $arg . " hello";
print "after concat with taint var -- ".(tainted($arg)?"is ":"is not ")."tainted.\n";

$arg = func ($arg);
print "after call function with taint var -- ".(tainted($arg)?"is ":"is not ")."tainted.\n";

eval {
	eval "$arg";	# Error
};
if ($@) {
	print "Error during call dangerous function with taint variable -- OK\n";
} else {
	print "Call dangerous function with taint variable -- ERROR\n";
}

if ($arg =~ /(ls)\s(-l)/) {
    $arg = "$1 $2";
} else {
    $arg = ""; # successful match did not occur
}
print "arg after untaining -- ".(tainted($arg)?"is ":"is not ")."tainted.\n";

eval {
	eval "$arg";	# Error
};
if ($@) {
	print "Error during call dangerous function with untaint variable -- ERROR\n";
} else {
	print "Call dangerous function with untaint variable -- OK\n";
}
