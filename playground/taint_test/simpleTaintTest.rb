
def addHello (str, str2)
	str + " Hello!"
end

puts "=== Taint tests ==="

puts "=== Tainted variable from ENV[\"HOME\"] -- true ==="
t1 = ENV["HOME"]
p t1.tainted?

puts "=== Expression with taint variable -- true ==="
t2 = "puts \"" +  t1 + "\""
puts t2.tainted?

puts "=== Call function with using taint parameter -- true ==="
puts addHello(t1, "Just String").tainted?

puts "=== Call function without using taint parameter -- false ==="
puts addHello("Just String", t1).tainted?

puts "=== Call dangerous method with taint value and $SAFE=0 -- Success ==="
begin
	eval(t2)
rescue SecurityError
	p "Security Error"
end

puts "=== Call dangerous method with taint value and $SAFE=1 -- Exception ==="
$SAFE = 1
begin
	eval(t2)
rescue SecurityError
	p "Security Error"
end

puts "=== Call dangerous method with taint value after untaining -- Sucess ==="
begin
	eval(t2.untaint)
rescue SecurityError
	p "Security Error"
end

