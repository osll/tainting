#ifndef CONFIGTAINTCHECKER_H
#define CONFIGTAINTCHECKER_H

/**
 * Config file parser for Taint Checker
 * Format:
 * # comment
 * [AddTaintingRule]
 * # rules for functions which add taint
 * name = fun1      #name of function
 * args = 0, 1      #numbers of args which should be tainted
 * ret_val = true   # true if return value tainted
 * name = fun2
 * args = 0
 * ret_val = false
 * ...
 *
 * [TaintPropagationRules]
 * # rules for taint propagation through functions
 * name = fun1      # name of function
 * src_args = 0, 1  # numbers of args which should be tainted if destination args are tainted
 * dst_args = 2, 3
 * ret_val = false  # true if return value could be tainted
 *
 * [UntaintingRules]
 * # rules for functions which delete taint
 * name= fun1       # name of function
 * args = 0, 1      # numbers of arguments, if one of them is tainted - generate error
 * ret_val = true   # true if return value is untainted
 *
 * [TaintingErrorRules]
 * name= fun1       # name of function
 * args = 0, 1      # numbers of arguments, if one of them is tainted - generate error
 *
 * [EscapeSequencesRules]
 *  <escape sequence> = <replaced sequence>
 * ...
 *
 */

#include "llvm/ADT/SmallVector.h"

#include <vector>
#include <map>
#include <fstream>
#include <iostream>

using namespace llvm;

class TaintConfigParser {
public:
    typedef SmallVector<unsigned, 2> ArgVector;
    typedef std::pair<std::string, std::string> EscapeSequencesRule;

    struct AddTaintingParam{
        ArgVector Args;
        bool TaintRet;
        AddTaintingParam(ArgVector Args, bool TaintRet):
            Args(Args), TaintRet(TaintRet) {
        }
    };
    struct TaintPropagationParam {
        ArgVector SrcArgs;
        ArgVector DstArgs;
        bool TaintRet;
        TaintPropagationParam(ArgVector SArgs, ArgVector DArgs, bool TaintRet):
            SrcArgs(SArgs), DstArgs(DArgs), TaintRet(TaintRet) {}
    };
    struct SanitizeParam {
        ArgVector Args;
        bool TaintRet;
        SanitizeParam(ArgVector Args,bool TaintRet):
            Args(Args), TaintRet(TaintRet) {}
    };
    struct TaintErrorParam{
        ArgVector Args;
        TaintErrorParam(ArgVector Args):
            Args(Args) {}
    };

    TaintConfigParser(): Rule(UNKNOWN) {}
    ~TaintConfigParser();
    void parse(std::string Filename);

    AddTaintingParam const *getAddTaintingParam(std::string const& Name) const;
    TaintPropagationParam const *getTaintPropagationParam(std::string const& Name) const;
    SanitizeParam const *getSanitizeParam(std::string const& Name) const;
    TaintErrorParam const *getTaintErrorParam(std::string const& Name) const;
    const std::map<std::string, std::string>& getReplaceSequences() const;

    AddTaintingParam const *findAddTaintingParam(std::string const& Name) const;
    TaintPropagationParam const *findTaintPropagationParam(std::string const& Name) const;
    SanitizeParam const *findSanitizeParam(std::string const& Name) const;
    TaintErrorParam const *findTaintErrorParam(std::string const& Name) const;
private:
    enum RuleType {
        UNKNOWN,
        ADD_TAINTING_RULE,
        PROPAGATION_RULE,
        UNTAINTING_RULE,
        TAINT_ERROR_RULE,
        ESCAPE_SEQUENCE_RULE
    };
    void clearRules();
    bool nextToken(std::ifstream& is, std::vector<std::string>& Tokens);
    void split(std::string const& Str, std::string const& Delim, std::vector<std::string>& Tokens);
    bool switchRule(std::vector<std::string> const& Tokens);
    std::string getName(std::vector<std::string> const& Tokens);
    ArgVector getArgs(std::vector<std::string> const& Tokens, std::string const& ArgName);
    int getRetVal(std::vector<std::string> const& Tokens);
    EscapeSequencesRule getEscapeSequenceRule(const std::vector<std::string> &Tokens);

    RuleType Rule;
    std::map<std::string, AddTaintingParam *> AddTaintingRules;
    std::map<std::string, TaintPropagationParam *> PropagationRules;
    std::map<std::string, SanitizeParam *> SanitizeRules;
    std::map<std::string, TaintErrorParam *> TaintErrorRules;
    std::map<std::string, std::string>  EscapeSequencesRules;
};

#endif // CONFIGTAINTCHECKER_H
