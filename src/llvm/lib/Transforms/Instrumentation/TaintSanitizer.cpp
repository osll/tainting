#define DEBUG_TYPE "taintsan"

#include "llvm/Transforms/Instrumentation.h"
#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/ADT/SmallString.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/ValueMap.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/InlineAsm.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/MDBuilder.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/InstVisitor.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Compiler.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/BlackList.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/Transforms/Utils/ModuleUtils.h"
#include "llvm/Support/ConfigTaintChecker.h"
using namespace llvm;

static const uint64_t kShadowMask32 = 1ULL << 31;
static const uint64_t kShadowMask64 = 1ULL << 46;
static const unsigned kShadowTLSAlignment = 8;

static cl::opt<bool> ClKeepGoing("taintsan-keep-going",
       cl::desc("keep going after reporting a taint error"),
       cl::Hidden, cl::init(false));

static cl::opt<bool> ClHandleICmp("taintsan-handle-icmp",
       cl::desc("propagate shadow through ICmpEQ and ICmpNE"),
       cl::Hidden, cl::init(true));

static cl::opt<bool> ClHandleICmpExact("taintsan-handle-icmp-exact",
       cl::desc("exact handling of relational integer ICmp"),
       cl::Hidden, cl::init(false));

static cl::opt<bool> ClPoisonStackWithCall("taintsan-add-taint-stack-with-call",
       cl::desc("add taint to stack variables with a call"),
       cl::Hidden, cl::init(false));
static cl::opt<int> ClPoisonStackPattern("taintsan-taint-stack-pattern",
       cl::desc("add taint to stack variables with the given patter"),
       cl::Hidden, cl::init(0xff));

static cl::opt<bool> ClCheckAccessAddress("taintsan-check-access-address",
       cl::desc("report accesses through a pointer which has poisoned shadow"),
       cl::Hidden, cl::init(true));

static cl::opt<std::string>  ClBlacklistFile("taintsan-blacklist",
       cl::desc("File containing the list of functions where TaintSanitizer "
                "should not report bugs"), cl::Hidden);

namespace {

/// \brief An instrumentation pass implementing detection of using tainted data.
///
/// TaintSanitizer: instrument the code of module for tainting analysis.
class TaintSanitizer : public FunctionPass {
public:
    TaintSanitizer(StringRef BlackListFile = StringRef()) :
        FunctionPass(ID),
        TD(0),
        WarningFn(0),BlacklistFile(BlackListFile.empty() ?
                ClBlacklistFile : BlackListFile) {}
    const char *getPassName() const { return "TaintSanitizer"; }
    bool runOnFunction(Function &F);
    bool doInitialization(Module &M);
    static char ID;   // Pass identification, replacement for typeid.

private:
    void initializeCallbacks (Module &M);

    DataLayout *TD;
    LLVMContext *C;
    Type *IntptrTy;
    Type *OriginTy; // delete
    /// \brief Thread-local shadow storage for function parameters.
    GlobalVariable *ParamTLS;
    /// \brief Thread-local shadow storage for function return value
    GlobalVariable *RetvalTLS;
    /// \brief Thread-local shadow storage for in-register va_arg function
    /// parameters (x86_64-specific).
    GlobalVariable *VAArgTLS;
    /// \brief Thread-local shadow storage for va_arg overflow area
    /// (x86_64-specific).
    GlobalVariable *VAArgOverflowSizeTLS;
    /// \brief The run-time callback to print a warning.
    Value *WarningFn;
    /// \brief Run-time helper that add taint to stack on function entry.
    Value * TaintsanAddTaintToStackFn;
    /// \brief Taintsan runtime replacements for memmove, memcpy and memset.
    Value *MemmoveFn, *MemcpyFn, *MemsetFn;

    /// \brief Address mask used in application-to-shadow addres calculation
    /// ShadowAddr is computed as ShadowAddr & ~3ULL
    uint64_t ShadowMask;
    /// \brief Branch weights for error reporting.
    MDNode *ColdCallWeights;
    /// \brief Path to Blacklist file.
    SmallString<64> BlacklistFile;
    /// \brief The Blacklist.
    OwningPtr<BlackList> BL;
    /// \brief An empty volatile inline asm that prevents callback merge.
    InlineAsm *EmptyAsm;

    friend struct TaintSanitizerVisitor;
    friend struct VarArgAMD64Helper;
};
} // namespace

char TaintSanitizer::ID = 0;

INITIALIZE_PASS(TaintSanitizer, "taintsan",
        "TaintSanitizer: tainting tracking.", false, false)

FunctionPass *llvm::createTaintSanitizerPass(StringRef BlacklistFile) {
    return new TaintSanitizer(BlacklistFile);
}

/// \brief Insert extern declaration of runtime-provided functions and globals.
void TaintSanitizer::initializeCallbacks(Module &M) {
  // Only do this once.
  if (WarningFn)
    return;

  IRBuilder<> IRB(*C);
  // Create the callback.
  StringRef WarningFnName = ClKeepGoing ? "__taintsan_warning"
                                        : "__taintsan_warning_noreturn";
  WarningFn = M.getOrInsertFunction(WarningFnName, IRB.getVoidTy(), IRB.getInt8PtrTy(), NULL);

  TaintsanAddTaintToStackFn = M.getOrInsertFunction(
    "__taintsan_add_taint_stack", IRB.getVoidTy(), IRB.getInt8PtrTy(), IntptrTy, NULL);
  MemmoveFn = M.getOrInsertFunction(
    "__taintsan_memmove", IRB.getInt8PtrTy(), IRB.getInt8PtrTy(),
    IRB.getInt8PtrTy(), IntptrTy, NULL);
  MemcpyFn = M.getOrInsertFunction(
    "__taintsan_memcpy", IRB.getInt8PtrTy(), IRB.getInt8PtrTy(), IRB.getInt8PtrTy(),
    IntptrTy, NULL);
  MemsetFn = M.getOrInsertFunction(
    "__taintsan_memset", IRB.getInt8PtrTy(), IRB.getInt8PtrTy(), IRB.getInt32Ty(),
    IntptrTy, NULL);

  // Create globals.
  RetvalTLS = new GlobalVariable(
    M, ArrayType::get(IRB.getInt64Ty(), 8), false,
    GlobalVariable::ExternalLinkage, 0, "__taintsan_retval_tls", 0,
    GlobalVariable::GeneralDynamicTLSModel);
  ParamTLS = new GlobalVariable(
    M, ArrayType::get(IRB.getInt64Ty(), 1000), false,
    GlobalVariable::ExternalLinkage, 0, "__taintsan_param_tls", 0,
    GlobalVariable::GeneralDynamicTLSModel);

  VAArgTLS = new GlobalVariable(
    M, ArrayType::get(IRB.getInt64Ty(), 1000), false,
    GlobalVariable::ExternalLinkage, 0, "__taintsan_va_arg_tls", 0,
    GlobalVariable::GeneralDynamicTLSModel);
  VAArgOverflowSizeTLS = new GlobalVariable(
    M, IRB.getInt64Ty(), false, GlobalVariable::ExternalLinkage, 0,
    "__taintsan_va_arg_overflow_size_tls", 0,
    GlobalVariable::GeneralDynamicTLSModel);

  // We insert an empty inline asm after __taintsan_report* to avoid callback merge.
  EmptyAsm = InlineAsm::get(FunctionType::get(IRB.getVoidTy(), false),
                            StringRef(""), StringRef(""),
                            /*hasSideEffects=*/true);

}

bool TaintSanitizer::doInitialization(Module &M) {
    TD = getAnalysisIfAvailable<DataLayout>();
    if (!TD)
        return false;
    BL.reset(new BlackList(BlacklistFile));
    C = &(M.getContext());
    unsigned PtrSize = TD->getPointerSizeInBits(0);
    switch (PtrSize) {
    case 64:
        ShadowMask = kShadowMask64;
        break;
    case 32:
        ShadowMask = kShadowMask32;
        break;
    default:
        report_fatal_error("unsupported pointer size");
        break;
    }

    IRBuilder<> IRB(*C);
    IntptrTy = IRB.getIntPtrTy(TD);

    ColdCallWeights = MDBuilder(*C).createBranchWeights(1, 1000);

    appendToGlobalCtors(M, cast<Function>(M.getOrInsertFunction(
            "__taintsan_init", IRB.getVoidTy(), NULL)), 0);

    new GlobalVariable(M, IRB.getInt32Ty(), true, GlobalValue::WeakODRLinkage,
            IRB.getInt32(ClKeepGoing), "__taintsan_keep_going");

    return true;
}

namespace {

/// \brief A helper class that handles instrumentation of VarArg
/// functions on a particular platform.
///
/// Implementations are expected to insert the instrumentation
/// necessary to propagate argument shadow through VarArg function
/// calls. Visit* methods are called during an InstVisitor pass over
/// the function, and should avoid creating new basic blocks. A new
/// instance of this class is created for each instrumented function.
struct VarArgHelper {
  /// \brief Visit a CallSite.
  virtual void visitCallSite(CallSite &CS, IRBuilder<> &IRB) = 0;

  /// \brief Visit a va_start call.
  virtual void visitVAStartInst(VAStartInst &I) = 0;

  /// \brief Visit a va_copy call.
  virtual void visitVACopyInst(VACopyInst &I) = 0;

  /// \brief Finalize function instrumentation.
  ///
  /// This method is called after visiting all interesting (see above)
  /// instructions in a function.
  virtual void finalizeInstrumentation() = 0;

  virtual ~VarArgHelper() {}
};

struct TaintSanitizerVisitor;

VarArgHelper *
CreateVarArgHelper(Function &Func, TaintSanitizer &Taintsan,
        TaintSanitizerVisitor &Visitor);

/// This class does all the work for a given function. Store and Load
/// instructions store and load corresponding shadow values.
/// Most instructions propagate shadow from arguments to their
/// return values.
struct TaintSanitizerVisitor : public InstVisitor<TaintSanitizerVisitor> {
    Function &F;
    TaintSanitizer &TS;
    SmallVector<PHINode *, 16> ShadowPHINodes;
    ValueMap<Value*, Value *> ShadowMap;
    bool InsertChecks;
    bool LoadShadow;
    OwningPtr<VarArgHelper> VAHelper;
    TaintConfigParser ConfigParser;

    struct ShadowAndInsertPoint {
        Value *Shadow;
        Instruction *OrigIns;
        ShadowAndInsertPoint(Value *S, Instruction *I): Shadow(S), OrigIns(I) { }
        ShadowAndInsertPoint(): Shadow(0), OrigIns(0) { }
    };
    SmallVector<ShadowAndInsertPoint, 16> InstrumentationList;
    SmallVector<Instruction *, 16> StoreList;

    TaintSanitizerVisitor(Function &F, TaintSanitizer &TS) :
        F(F), TS(TS), VAHelper(CreateVarArgHelper(F, TS, *this)) {
        LoadShadow = InsertChecks = !TS.BL->isIn(F) &&
                F.getAttributes().hasAttribute(AttributeSet::FunctionIndex,
                        Attribute::SanitizeTaint);
        DEBUG(if (!InsertChecks)
                  dbgs() << "TaintSanitizer is not inserting checks into '"
                         << F.getName() << "'\n");
        ConfigParser.parse("rules.cfg");
    }
    void materializeStores() {
        for (size_t i = 0, n = StoreList.size(); i < n; ++i) {
            StoreInst& I = *dyn_cast<StoreInst>(StoreList[i]);

            IRBuilder<> IRB(&I);
            Value *Val = I.getValueOperand();
            Value *Addr = I.getPointerOperand();
            Value *Shadow = getShadow(Val);
            Value *ShadowPtr = getShadowPtr(Addr, Shadow->getType(), IRB);

            StoreInst *NewSI =
                    IRB.CreateAlignedStore(Shadow, ShadowPtr, I.getAlignment());
            DEBUG (dbgs() << " STORE: " << NewSI << "\n");
            (void)NewSI;

            if (ClCheckAccessAddress)
                insertCheck(Addr, &I);
        }
    }

    void materializeChecks() {
        for (size_t i = 0, n = InstrumentationList.size(); i < n; ++i) {
            Value *Shadow = InstrumentationList[i].Shadow;
            Instruction *OrigIns = InstrumentationList[i].OrigIns;
            IRBuilder<> IRB(OrigIns);
            DEBUG(dbgs() << "SHAD0: " << *Shadow << "\n");
            Value *ConvertedShadow = convertToShadowTyNoVec(Shadow, IRB);
            DEBUG(dbgs() << "SHAD1: " << *ConvertedShadow << "\n");
            ICmpInst *Cmp = new ICmpInst(OrigIns, ICmpInst::ICMP_NE, ConvertedShadow,
                                         getCleanShadow(ConvertedShadow), "_tscmp");
            Instruction *CheckTerm =
                    SplitBlockAndInsertIfThen(cast<Instruction>(Cmp),
                            !ClKeepGoing, TS.ColdCallWeights);
            IRB.SetInsertPoint(CheckTerm);
            Value * FuncName;
            if (InvokeInst *II = dyn_cast<InvokeInst>(OrigIns)) {
                FuncName = IRB.CreateGlobalString(II->getCalledFunction()->getName());
            } else if (CallInst * CI = dyn_cast<CallInst>(OrigIns)) {
                FuncName = IRB.CreateGlobalString(CI->getCalledFunction()->getName());
            } else {
                FuncName = IRB.CreateGlobalString("");
            }
            CallInst *Call = IRB.CreateCall(TS.WarningFn, FuncName);
            Call->setDebugLoc(OrigIns->getDebugLoc());
            IRB.CreateCall(TS.EmptyAsm);
            DEBUG(dbgs() << "CHECK: "<< *Cmp << "\n");
        }
        DEBUG(dbgs() << "DONE:\n" << F);

    }

    /// \brief Add TaintSanitizer instrumentation to a function.
    bool runOnFunction() {
        TS.initializeCallbacks(*F.getParent());
        if(!TS.TD)
            return false;
        // In the presence of unreachable blocks, we may see Phi nodes with
        // incoming nodes from such blocks. Since InstVisitor skips unreachable
        // blocks, such nodes will not have any shadow value associated with them.
        // It's easier to remove unreachable blocks than deal with missing shadow.
        removeUnreachableBlocks(F);

        for (df_iterator<BasicBlock *> DI = df_begin(&F.getEntryBlock()),
                DE = df_end(&F.getEntryBlock()); DI != DE; ++DI) {
            BasicBlock *BB = *DI;
            visit(*BB);
        }

        // Finalize PHI nodes
        for (size_t i = 0, n = ShadowPHINodes.size(); i < n; ++i) {
            PHINode *PN = ShadowPHINodes[i];
            PHINode *PNS = cast<PHINode>(getShadow(PN));
            size_t NumValues = PN->getNumIncomingValues();
            for (size_t v = 0; v < NumValues; ++v) {
                PNS->addIncoming(getShadow(PN, v), PN->getIncomingBlock(v));
            }
        }

        VAHelper->finalizeInstrumentation();

        // Delayed instrumentation of StoreInst.
        // This may add new checks to be inserted later.
        materializeStores();

        // Insert shadow value checks.
        materializeChecks();

        return true;
    }

    /// \brief Compute the shadow type that corresponds to a give Value.
    Type *getShadowTy(Value *V) {
        return getShadowTy(V->getType());
    }

    /// \brief Compute the shadow type that corresponds to a given Type.
    Type *getShadowTy(Type *OrigTy) {
        if (!OrigTy->isSized()) {
            return 0;
        }
        if(IntegerType *IT = dyn_cast<IntegerType>(OrigTy)) {
            return IT;
        }
        if (VectorType *VT = dyn_cast<VectorType>(OrigTy)) {
            uint EltSize = TS.TD->getTypeSizeInBits(VT->getElementType());
            return VectorType::get(IntegerType::get(*TS.C, EltSize),
                    VT->getNumElements());
        }
        if (StructType *ST = dyn_cast<StructType>(OrigTy)) {
            SmallVector<Type*, 4> Elements;
            for (unsigned i = 0, n = ST->getNumElements(); i < n; i++)
                Elements.push_back(getShadowTy(ST->getElementType(i)));
            StructType *Res = StructType::get(*TS.C, Elements, ST->isPacked());
            DEBUG(dbgs() << "getShadowTy: " << *ST << " ===> " << *Res << "\n");
            return Res;
        }
        uint32_t TypeSize = TS.TD->getTypeSizeInBits(OrigTy);
        return IntegerType::get(*TS.C, TypeSize);

    }

    /// \brief Flatten a vector type
    Type *getShadowTyNoVec(Type *Ty) {
        if (VectorType *VT = dyn_cast<VectorType>(Ty))
            return IntegerType::get(*TS.C, VT->getBitWidth());
        return Ty;
    }

    /// \brief Convert a shadow value to it's flattened variant.
    Value *convertToShadowTyNoVec(Value *V, IRBuilder<> &IRB) {
        Type *Ty = V->getType();
        Type *NoVecTy = getShadowTyNoVec(Ty);
        if (Ty == NoVecTy)
            return V;
        return IRB.CreateBitCast(V, NoVecTy);
    }

    /// \brief Compute thre shadow address that corresponds to a given application
    /// address.
    ///
    /// Shadow = Addr & ~ShadowMask.
    Value *getShadowPtr (Value *Addr, Type *ShadowTy, IRBuilder<> &IRB) {
        Value *ShadowPtr = IRB.CreateAnd(IRB.CreatePointerCast(Addr, TS.IntptrTy),
                ConstantInt::get(TS.IntptrTy, ~TS.ShadowMask));
        return IRB.CreateIntToPtr(ShadowPtr, PointerType::get(ShadowTy, 0));
    }
    /// \brief Compute the Shadow address for a given function argument.
    ///
    /// Shadow = ParamTLS + ArgOffset.
    Value *getShadowPtrForArgument(Value *A, IRBuilder<> &IRB, int ArgOffset) {
        Value *Base = IRB.CreatePointerCast(TS.ParamTLS, TS.IntptrTy);
        Base = IRB.CreateAdd(Base, ConstantInt::get(TS.IntptrTy, ArgOffset));
        PointerType::get(getShadowTy(A), 0);
        return IRB.CreateIntToPtr(Base, PointerType::get(getShadowTy(A), 0),
                "_tsarg");
    }

    /// \brief Compute the Shadow address for a retval.
    Value *getShadowPtrForRetval(Value *A, IRBuilder<> &IRB) {
        Value *Base = IRB.CreatePointerCast(TS.RetvalTLS, TS.IntptrTy);
        return IRB.CreateIntToPtr(Base, PointerType::get(getShadowTy(A), 0),
                "_tsret");
    }

    /// \brief Set SV to be  the shadow value for V.
    void setShadow(Value *V, Value *SV) {
        assert (!ShadowMap.count(V) && "Values may only have one shadow");
        ShadowMap[V] = SV;
    }

    /// \brief Create a clean shadow value for a given value.
    ///
    /// Clean shadow means all bits of the value are untainted.
    Constant *getCleanShadow(Value *V) {
        Type *ShadowTy = getShadowTy(V);
        if (!ShadowTy)
            return 0;
        return Constant::getNullValue(ShadowTy);
    }

    /// \brief Create a dirty shadow of a given shadow type.
    Constant *getTaintedShadow(Type *ShadowTy) {
        assert(ShadowTy);
        if (isa<IntegerType>(ShadowTy) || isa<VectorType>(ShadowTy)) {
            return Constant::getAllOnesValue(ShadowTy);
        }
        StructType *ST = cast<StructType>(ShadowTy);
        SmallVector<Constant *, 4> Vals;
        for (unsigned i = 0, n = ST->getNumElements(); i < n; ++i) {
            Vals.push_back(getTaintedShadow(ST->getElementType(i)));
        }
        return ConstantStruct::get(ST, Vals);
    }

    /// \brief Create a tainted shadow for a given value.
    Constant *getTaintedShadow(Value *V) {
        Type *ShadowTy = getShadowTy(V);
        if(!ShadowTy) {
            return 0;
        }
        return getTaintedShadow(ShadowTy);
    }

    /// \brief Get the shadow value for a given Value.
    ///
    /// This is function either returns the shadow value for a given Value.
    /// or extracts it from ParamTLS (for function arguments).
    Value *getShadow(Value *V) {
        if (Instruction *I = dyn_cast<Instruction>(V)) {
            // For instructions the shadow is already stored in the map.
            Value *Shadow = ShadowMap[V];
            if(!Shadow) {
                DEBUG(dbgs() << "No shadow: " << *V << "\n"
                        << *(I->getParent()));
                (void)I;
                assert(Shadow && "No shadow for a value");
            }
            return Shadow;
        }
        if (UndefValue *U = dyn_cast<UndefValue>(V)) {
            Value *Shadow = getCleanShadow(V);
            DEBUG(dbgs() << "Undef: " << *U << "==>" << *Shadow << "\n");
            (void)U;
            return Shadow;
        }
        if (Argument *A = dyn_cast<Argument>(V)) {
            // For arguments we compute the shadow on demand and store it in the map.
            Value **ShadowPtr = &ShadowMap[V];
            if (*ShadowPtr)
                return *ShadowPtr;
            Function *F = A->getParent();
            IRBuilder<> EntryIRB(F->getEntryBlock().getFirstNonPHI());
            unsigned ArgOffset = 0;
            for (Function::arg_iterator AI = F->arg_begin(), AE = F->arg_end();
                    AI != AE; ++AI) {
                if (!AI->getType()->isSized()) {
                    DEBUG(dbgs() << "Arg is not sized\n");
                    continue;
                }
                unsigned Size = AI->hasByValAttr()
                        ? TS.TD->getTypeAllocSize(AI->getType()->getPointerElementType())
                        : TS.TD->getTypeAllocSize(AI->getType());
                if (A == AI) {
                    Value *Base = getShadowPtrForArgument(AI, EntryIRB, ArgOffset);
                    if (AI->hasByValAttr()) {
                        // ByVal pointer itself has clean shadow. We copy the actual
                        // argument shadow to the underlying memory.
                        Value *Cpy = EntryIRB.CreateMemCpy(
                            getShadowPtr(V, EntryIRB.getInt8Ty(), EntryIRB),
                            Base, Size, AI->getParamAlignment());
                        DEBUG(dbgs() << " ByValCpy: " << *Cpy << "\n");
                        (void)Cpy;
                        *ShadowPtr = getCleanShadow(V);
                    } else {
                        *ShadowPtr = EntryIRB.CreateLoad(Base);
                    }
                    DEBUG(dbgs() << " ARG:  " << *AI << " ==> "
                            << **ShadowPtr << "\n");
                }
                ArgOffset += DataLayout::RoundUpAlignment(Size, 8);
            }
            assert(*ShadowPtr && "Could not find shadow for an argument");
            return *ShadowPtr;
        }
        return getCleanShadow(V);
    }

    /// \brief Get the Shadow for i-th argument of the instruction I.
    Value *getShadow(Instruction *I, int i) {
        return  getShadow(I->getOperand(i));
    }

    /// \brief Remember the place where a shadow check should be inserted.
    ///
    /// This location will be later instrumented with a check that will print a
    /// Taint error in runtime if the value is not fully defined.
    void insertCheck(Value *Val, Instruction *OrigIns) {
        assert(Val);
        if(!InsertChecks) return;
        Value *Shadow = getShadow(Val);
        if (!Shadow) {
            return;
        }
#ifndef NDEBUG
        Type *ShadowTy = Shadow->getType();
        assert((isa<IntegerType>(ShadowTy) || isa<VectorType>(ShadowTy)) &&
                "Can only insert checks for integer and vector shadow types");
#endif
        InstrumentationList.push_back(ShadowAndInsertPoint(Shadow, OrigIns));
    }

    // ------------------Visitors

    /// \brief Instrument LoadInst
    ///
    /// Loads the corresponding shadow
    void visitLoadInst(LoadInst &I) {
        assert(I.getType()->isSized() && "Load type must have size");
        IRBuilder<> IRB(&I);
        Type *ShadowTy = getShadowTy(&I);
        Value *Addr = I.getPointerOperand();
        if (LoadShadow) {
            Value *ShadowPtr = getShadowPtr(Addr, ShadowTy, IRB);
            setShadow(&I,
                    IRB.CreateAlignedLoad(ShadowPtr, I.getAlignment(), "_stld"));
        } else {
            setShadow(&I, getCleanShadow(&I));
        }

        if (ClCheckAccessAddress)
            insertCheck(I.getPointerOperand(), &I);
    }

    /// \brief Instrument StoreInst
    ///
    /// Stores the corresponding shadow
    void visitStoreInst(StoreInst &I) {
        StoreList.push_back(&I);
    }

    void visitExtractElementInst(ExtractElementInst &I) {
        insertCheck(I.getOperand(1), &I);
        IRBuilder<> IRB(&I);
        setShadow(&I, IRB.CreateExtractElement(getShadow(&I, 0), I.getOperand(1),
                "_tsprop"));
    }

    void visitInsertElementInst(InsertElementInst &I) {
        insertCheck(I.getOperand(2), &I);
        IRBuilder<> IRB(&I);
        setShadow(&I, IRB.CreateInsertElement(getShadow(&I, 0),
                getShadow(&I, 1), I.getOperand(2), "_tsprop"));
    }

    void visitShuffleVectorInst(ShuffleVectorInst &I) {
        insertCheck(I.getOperand(2), &I);
        IRBuilder<> IRB(&I);
        setShadow(&I, IRB.CreateShuffleVector(getShadow(&I, 0), getShadow(&I, 1),
                      I.getOperand(2), "_tsprop"));
    }

    // Casts.
    void visitSExtInst(SExtInst &I) {
        IRBuilder<> IRB(&I);
        setShadow(&I, IRB.CreateSExt(getShadow(&I, 0), I.getType(), "_tsprop"));
    }

    void visitZExtInst(ZExtInst &I) {
        IRBuilder<> IRB(&I);
        setShadow(&I, IRB.CreateZExt(getShadow(&I, 0), I.getType(), "_tsprop"));
    }

    void visitTruncInst(TruncInst &I) {
        IRBuilder<> IRB(&I);
        setShadow(&I, IRB.CreateTrunc(getShadow(&I, 0), I.getType(), "_tsprop"));
    }

    void visitBitCastInst(BitCastInst &I) {
        IRBuilder<> IRB(&I);
        setShadow(&I, IRB.CreateBitCast(getShadow(&I, 0), getShadowTy(&I)));
    }

    void visitPtrToIntInst(PtrToIntInst &I) {
        IRBuilder<> IRB(&I);
        setShadow(&I, IRB.CreateIntCast(getShadow(&I, 0), getShadowTy(&I), false,
                 "_tsprop_ptrtoint"));
    }

    void visitIntToPtrInst(IntToPtrInst &I) {
        IRBuilder<> IRB(&I);
        setShadow(&I, IRB.CreateIntCast(getShadow(&I, 0), getShadowTy(&I), false,
                 "_tsprop_inttoptr"));
    }

    void visitFPToSIInst(CastInst& I) { handleShadowOr(I); }
    void visitFPToUIInst(CastInst& I) { handleShadowOr(I); }
    void visitSIToFPInst(CastInst& I) { handleShadowOr(I); }
    void visitUIToFPInst(CastInst& I) { handleShadowOr(I); }
    void visitFPExtInst(CastInst& I) { handleShadowOr(I); }
    void visitFPTruncInst(CastInst& I) { handleShadowOr(I); }

    /// \brief Propagate shadow for bitwise AND.
    ///
    /// This code is exact, i.e. if, for example, a bit in the left argument
    /// is defined and 0, then neither the value not definedness of the
    /// corresponding bit in B don't affect the resulting shadow.
    void visitAnd(BinaryOperator &I) {
        IRBuilder<> IRB(&I);
        Value *S1 = getShadow(&I, 0);
        Value *S2 = getShadow(&I, 1);
        Value *V1 = I.getOperand(0);
        Value *V2 = I.getOperand(1);
        if (V1->getType() != S1->getType()) {
            V1 = IRB.CreateIntCast(V1, S1->getType(), false);
            V2 = IRB.CreateIntCast(V2, S2->getType(), false);
        }
        Value *S1S2 = IRB.CreateAnd(S1, S2);
        Value *V1S2 = IRB.CreateAnd(V1, S2);
        Value *S1V2 = IRB.CreateAnd(S1, V2);
        setShadow(&I, IRB.CreateOr(S1S2, IRB.CreateOr(V1S2, S1V2)));
    }

    void visitOr(BinaryOperator &I) {
        IRBuilder<> IRB(&I);
        Value *S1 = getShadow(&I, 0);
        Value *S2 = getShadow(&I, 1);
        Value *V1 = IRB.CreateNot(I.getOperand(0));
        Value *V2 = IRB.CreateNot(I.getOperand(1));
        if (V1->getType() != S1->getType()) {
            V1 = IRB.CreateIntCast(V1, S1->getType(), false);
            V2 = IRB.CreateIntCast(V2, S2->getType(), false);
        }
        Value *S1S2 = IRB.CreateAnd(S1, S2);
        Value *V1S2 = IRB.CreateAnd(V1, S2);
        Value *S1V2 = IRB.CreateAnd(S1, V2);
        setShadow(&I, IRB.CreateOr(S1S2, IRB.CreateOr(V1S2, S1V2)));
    }


    /// \brief Default propagation of shadow and/or origin.
    ///
    /// This class implements the general case of shadow propagation, used in all
    /// cases where we don't know and/or don't care about what the operation
    /// actually does. It converts all input shadow values to a common type
    /// (extending or truncating as necessary), and bitwise OR's them.
    ///
    /// This is much cheaper than inserting checks (i.e. requiring inputs to be
    /// fully initialized), and less prone to false positives.
    class Combiner {
        Value *Shadow;
        IRBuilder<> &IRB;
        TaintSanitizerVisitor *TSV;

    public:
        Combiner(TaintSanitizerVisitor *TSV, IRBuilder<> &IRB) :
            Shadow(0), IRB(IRB), TSV(TSV) {}

        /// \brief Add an application value to the mix.
        Combiner &Add(Value *V) {
            Value *OpShadow = TSV->getShadow(V);
            assert(OpShadow);
            if (!OpShadow) return *this;
            if (!Shadow) {
                Shadow = OpShadow;
            } else {
                Shadow->getType();
                OpShadow = TSV->CreateShadowCast(IRB, OpShadow, Shadow->getType());
                Shadow = IRB.CreateOr(Shadow, OpShadow, "_tsprop");
            }
            return *this;
        }

        /// \brief Set the current combined values as the given instruction's shadow
        void Done(Value *I) {
            assert(Shadow);
            Type * InstTy = TSV->getShadowTy(I);
            if (InstTy) {
                Shadow = TSV->CreateShadowCast(IRB, Shadow, TSV->getShadowTy(I));
                TSV->setShadow(I, Shadow);
            }
        }
    };

    size_t VectorOrPrimitiveTypeSizeInBits(Type *Ty) {
        assert(!(Ty->isVectorTy() && Ty->getScalarType()->isPointerTy()) &&
               "Vector of pointers is not a valid shadow type");
        return Ty->isVectorTy() ?
                Ty->getVectorNumElements() * Ty->getScalarSizeInBits() :
                Ty->getPrimitiveSizeInBits();
    }

    /// \brief Cast between two shadow types, extending or truncating as
    /// necessary.
    Value *CreateShadowCast(IRBuilder<> &IRB, Value *V, Type *dstTy) {
        Type *srcTy = V->getType();
        bool itt = dstTy->isIntegerTy();
        if (itt && srcTy->isIntegerTy()) {
            return IRB.CreateIntCast(V, dstTy, false);
        }
        if (dstTy->isVectorTy() && srcTy->isVectorTy() &&
                dstTy->getVectorNumElements() == srcTy->getVectorNumElements())
            return IRB.CreateIntCast(V, dstTy, false);
        size_t srcSizeInBits = VectorOrPrimitiveTypeSizeInBits(srcTy);
        size_t dstSizeInBits = VectorOrPrimitiveTypeSizeInBits(dstTy);
        Value *V1 = IRB.CreateBitCast(V, Type::getIntNTy(*TS.C, srcSizeInBits));
        Value *V2 =
                IRB.CreateIntCast(V1, Type::getIntNTy(*TS.C, dstSizeInBits), false);
        return IRB.CreateBitCast(V2, dstTy);

    }

    /// \brief Propagate shadow for arbitrary operation.
    void handleShadowOr(Instruction &I) {
        IRBuilder<> IRB(&I);
        Combiner SC(this, IRB);
        for (Instruction::op_iterator OI = I.op_begin(); OI != I.op_end(); ++OI) {
            if (OI->get()->getType()->isLabelTy()) continue;
            SC.Add(OI->get());
        }
        SC.Done(&I);
    }

    void visitFAdd(BinaryOperator &I) { handleShadowOr(I); }
    void visitFSub(BinaryOperator &I) { handleShadowOr(I); }
    void visitFMul(BinaryOperator &I) { handleShadowOr(I); }
    void visitAdd(BinaryOperator &I) { handleShadowOr(I); }
    void visitSub(BinaryOperator &I) { handleShadowOr(I); }
    void visitXor(BinaryOperator &I) { handleShadowOr(I); }
    void visitMul(BinaryOperator &I) { handleShadowOr(I); }

    void handleDiv(Instruction &I) {
        IRBuilder<> IRB(&I);
        // Strict on the second argument
        setShadow(&I, getShadow(&I, 0));
    }

    void visitUDiv(BinaryOperator &I) { handleDiv(I); }
    void visitSDiv(BinaryOperator &I) { handleDiv(I); }
    void visitFDiv(BinaryOperator &I) { handleDiv(I); }
    void visitURem(BinaryOperator &I) { handleDiv(I); }
    void visitSRem(BinaryOperator &I) { handleDiv(I); }
    void visitFRem(BinaryOperator &I) { handleDiv(I); }

    /// \brief Instrument == and != comparisons.
    ///
    /// Sometimes the comparison result is known even if some of the bits of the
    /// arguments are not.
    void handleEqualityComparison(ICmpInst &I) {
        IRBuilder<> IRB(&I);
        Value *A = I.getOperand(0);
        Value *B = I.getOperand(1);
        Value *Sa = getShadow(A);
        Value *Sb = getShadow(B);

        A = IRB.CreatePointerCast(A, Sa->getType());
        B = IRB.CreatePointerCast(B, Sb->getType());

        // A == B  <==>  (C = A^B) == 0
        // A != B  <==>  (C = A^B) != 0
        // Sc = Sa | Sb
        Value *C = IRB.CreateXor(A, B);
        Value *Sc = IRB.CreateOr(Sa, Sb);
        // Now dealing with i = (C == 0) comparison (or C != 0, does not matter now)
        // Result is defined if one of the following is true
        // * there is a defined 1 bit in C
        // * C is fully defined
        // Si = !(C & ~Sc) && Sc
        Value *Zero = Constant::getNullValue(Sc->getType());
        Value *MinusOne = Constant::getAllOnesValue(Sc->getType());
        Value *Si =
                IRB.CreateAnd(IRB.CreateICmpNE(Sc, Zero),
                        IRB.CreateICmpEQ(
                                IRB.CreateAnd(IRB.CreateXor(Sc, MinusOne), C), Zero));
        Si->setName("_tsprop_icmp");
        setShadow(&I, Si);
    }

    /// \brief Build the lowest possible value of V, taking into account V's
    ///        uninitialized bits.
    Value *getLowestPossibleValue(IRBuilder<> &IRB, Value *A, Value *Sa,
            bool isSigned) {
        if (isSigned) {
            // Split shadow into sign bit and other bits.
            Value *SaOtherBits = IRB.CreateLShr(IRB.CreateShl(Sa, 1), 1);
            Value *SaSignBit = IRB.CreateXor(Sa, SaOtherBits);
            // Maximise the undefined shadow bit, minimize other undefined bits.
            return IRB.CreateOr(IRB.CreateAnd(A, IRB.CreateNot(SaOtherBits)), SaSignBit);
        } else {
            // Minimize undefined bits.
            return IRB.CreateAnd(A, IRB.CreateNot(Sa));
        }
    }

    /// \brief Build the highest possible value of V, taking into account V's
    ///        uninitialized bits.
    Value *getHighestPossibleValue(IRBuilder<> &IRB, Value *A, Value *Sa,
            bool isSigned) {
        if (isSigned) {
            // Split shadow into sign bit and other bits.
            Value *SaOtherBits = IRB.CreateLShr(IRB.CreateShl(Sa, 1), 1);
            Value *SaSignBit = IRB.CreateXor(Sa, SaOtherBits);
            // Minimise the undefined shadow bit, maximise other undefined bits.
            return IRB.CreateOr(IRB.CreateAnd(A, IRB.CreateNot(SaSignBit)), SaOtherBits);
        } else {
            // Maximize undefined bits.
            return IRB.CreateOr(A, Sa);
        }
    }

    /// \brief Instrument relational comparisons.
    ///
    /// This function does exact shadow propagation for all relational
    /// comparisons of integers, pointers and vectors of those.
    void handleRelationalComparisonExact(ICmpInst &I) {
        IRBuilder<> IRB(&I);
        Value *A = I.getOperand(0);
        Value *B = I.getOperand(1);
        Value *Sa = getShadow(A);
        Value *Sb = getShadow(B);

        // Get rid of pointers and vectors of pointers.
        // For ints (and vectors of ints), types of A and Sa match,
        // and this is a no-op.
        A = IRB.CreatePointerCast(A, Sa->getType());
        B = IRB.CreatePointerCast(B, Sb->getType());

        // Let [a0, a1] be the interval of possible values of A, taking into account
        // its undefined bits. Let [b0, b1] be the interval of possible values of B.
        // Then (A cmp B) is defined iff (a0 cmp b1) == (a1 cmp b0).
        bool IsSigned = I.isSigned();
        Value *S1 = IRB.CreateICmp(I.getPredicate(),
                getLowestPossibleValue(IRB, A, Sa, IsSigned),
                getHighestPossibleValue(IRB, B, Sb, IsSigned));
        Value *S2 = IRB.CreateICmp(I.getPredicate(),
                getHighestPossibleValue(IRB, A, Sa, IsSigned),
                getLowestPossibleValue(IRB, B, Sb, IsSigned));
        Value *Si = IRB.CreateXor(S1, S2);
        setShadow(&I, Si);
    }

    /// \brief Instrument signed relational comparisons.
    ///
    /// Handle (x<0) and (x>=0) comparisons (essentially, sign bit tests) by
    /// propagating the highest bit of the shadow. Everything else is delegated
    /// to handleShadowOr().
    void handleSignedRelationalComparison(ICmpInst &I) {
        Constant *constOp0 = dyn_cast<Constant>(I.getOperand(0));
        Constant *constOp1 = dyn_cast<Constant>(I.getOperand(1));
        Value* op = NULL;
        CmpInst::Predicate pre = I.getPredicate();
        if (constOp0 && constOp0->isNullValue() &&
                (pre == CmpInst::ICMP_SGT || pre == CmpInst::ICMP_SLE)) {
            op = I.getOperand(1);
        } else if (constOp1 && constOp1->isNullValue() &&
                (pre == CmpInst::ICMP_SLT || pre == CmpInst::ICMP_SGE)) {
            op = I.getOperand(0);
        }
        if (op) {
            IRBuilder<> IRB(&I);
            Value* Shadow =
                    IRB.CreateICmpSLT(getShadow(op), getCleanShadow(op), "_tsprop_icmpslt");
            setShadow(&I, Shadow);
        } else {
            handleShadowOr(I);
        }
    }

    void visitICmpInst(ICmpInst &I) {
        if (!ClHandleICmp) {
            handleShadowOr(I);
            return;
        }
        if (I.isEquality()) {
            handleEqualityComparison(I);
            return;
        }

        assert(I.isRelational());
        if (ClHandleICmpExact) {
            handleRelationalComparisonExact(I);
            return;
        }
        if (I.isSigned()) {
            handleSignedRelationalComparison(I);
            return;
        }

        assert(I.isUnsigned());
        if ((isa<Constant>(I.getOperand(0)) || isa<Constant>(I.getOperand(1)))) {
            handleRelationalComparisonExact(I);
            return;
        }

        handleShadowOr(I);
    }

    void visitFCmpInst(FCmpInst &I) {
        handleShadowOr(I);
    }

    void handleShift(BinaryOperator &I) {
        IRBuilder<> IRB(&I);
        // If any of the S2 bits are tainted, the whole thing is tainted.
        // Otherwise perform the same shift on S1.
        Value *S1 = getShadow(&I, 0);
        Value *S2 = getShadow(&I, 1);
        Value *S2Conv = IRB.CreateSExt(IRB.CreateICmpNE(S2, getCleanShadow(S2)),
                S2->getType());
        Value *V2 = I.getOperand(1);
        Value *Shift = IRB.CreateBinOp(I.getOpcode(), S1, V2);
        setShadow(&I, IRB.CreateOr(Shift, S2Conv));
    }

    void visitShl(BinaryOperator &I) { handleShift(I); }
    void visitAShr(BinaryOperator &I) { handleShift(I); }
    void visitLShr(BinaryOperator &I) { handleShift(I); }

    /// \brief Instrument llvm.memmove
    ///
    /// At this point we don't know if llvm.memmove will be inlined or not.
    /// If we don't instrument it and it gets inlined,
    /// our interceptor will not kick in and we will lose the memmove.
    /// If we instrument the call here, but it does not get inlined,
    /// we will memove the shadow twice: which is bad in case
    /// of overlapping regions. So, we simply lower the intrinsic to a call.
    ///
    /// Similar situation exists for memcpy and memset.
    void visitMemMoveInst(MemMoveInst &I) {
        IRBuilder<> IRB(&I);
        IRB.CreateCall3(
                TS.MemmoveFn,
                IRB.CreatePointerCast(I.getArgOperand(0), IRB.getInt8PtrTy()),
                IRB.CreatePointerCast(I.getArgOperand(1), IRB.getInt8PtrTy()),
                IRB.CreateIntCast(I.getArgOperand(2), TS.IntptrTy, false));
        I.eraseFromParent();
    }

    // Similar to memmove: avoid copying shadow twice.
    // This is somewhat unfortunate as it may slowdown small constant memcpys.
    void visitMemCpyInst(MemCpyInst &I) {
        IRBuilder<> IRB(&I);
        IRB.CreateCall3(
                TS.MemcpyFn,
                IRB.CreatePointerCast(I.getArgOperand(0), IRB.getInt8PtrTy()),
                IRB.CreatePointerCast(I.getArgOperand(1), IRB.getInt8PtrTy()),
                IRB.CreateIntCast(I.getArgOperand(2), TS.IntptrTy, false));
        I.eraseFromParent();
    }

    // Same as memcpy.
    void visitMemSetInst(MemSetInst &I) {
        IRBuilder<> IRB(&I);
        IRB.CreateCall3(
                TS.MemsetFn,
                IRB.CreatePointerCast(I.getArgOperand(0), IRB.getInt8PtrTy()),
                IRB.CreateIntCast(I.getArgOperand(1), IRB.getInt32Ty(), false),
                IRB.CreateIntCast(I.getArgOperand(2), TS.IntptrTy, false));
        I.eraseFromParent();
    }

    void visitVAStartInst(VAStartInst &I) {
        VAHelper->visitVAStartInst(I);
    }

    void visitVACopyInst(VACopyInst &I) {
        VAHelper->visitVACopyInst(I);
    }

    enum IntrinsicKind {
        IK_DoesNotAccessMemory,
        IK_OnlyReadsMemory,
        IK_WritesMemory
    };

    static IntrinsicKind getIntrinsicKind(Intrinsic::ID iid) {
        const int DoesNotAccessMemory = IK_DoesNotAccessMemory;
        const int OnlyReadsArgumentPointees = IK_OnlyReadsMemory;
        const int OnlyReadsMemory = IK_OnlyReadsMemory;
        const int OnlyAccessesArgumentPointees = IK_WritesMemory;
        const int UnknownModRefBehavior = IK_WritesMemory;
#define GET_INTRINSIC_MODREF_BEHAVIOR
    #define ModRefBehavior IntrinsicKind
    #include "llvm/IR/Intrinsics.gen"
#undef ModRefBehavior
#undef GET_INTRINSIC_MODREF_BEHAVIOR
    }

    /// \brief Handle vector store-like intrinsics.
    ///
    /// Instrument intrinsics that look like a simple SIMD store: writes memory,
    /// has 1 pointer argument and 1 vector argument, returns void.
    bool handleVectorStoreIntrinsic(IntrinsicInst &I) {
        IRBuilder<> IRB(&I);
        Value* Addr = I.getArgOperand(0);
        Value *Shadow = getShadow(&I, 1);
        Value *ShadowPtr = getShadowPtr(Addr, Shadow->getType(), IRB);

        // We don't know the pointer alignment (could be unaligned SSE store!).
        // Have to assume to worst case.
        IRB.CreateAlignedStore(Shadow, ShadowPtr, 1);

        if (ClCheckAccessAddress)
            insertCheck(Addr, &I);

        return true;
    }

    /// \brief Handle vector load-like intrinsics.
    ///
    /// Instrument intrinsics that look like a simple SIMD load: reads memory,
    /// has 1 pointer argument, returns a vector.
    bool handleVectorLoadIntrinsic(IntrinsicInst &I) {
        IRBuilder<> IRB(&I);
        Value *Addr = I.getArgOperand(0);

        Type *ShadowTy = getShadowTy(&I);
        if (LoadShadow) {
            Value *ShadowPtr = getShadowPtr(Addr, ShadowTy, IRB);
            // We don't know the pointer alignment (could be unaligned SSE load!).
            // Have to assume to worst case.
            setShadow(&I, IRB.CreateAlignedLoad(ShadowPtr, 1, "_tsld"));
        } else {
            setShadow(&I, getCleanShadow(&I));
        }

        if (ClCheckAccessAddress)
            insertCheck(Addr, &I);

        return true;
    }

    /// \brief Handle (SIMD arithmetic)-like intrinsics.
    ///
    /// Instrument intrinsics with any number of arguments of the same type,
    /// equal to the return type. The type should be simple (no aggregates or
    /// pointers; vectors are fine).
    /// Caller guarantees that this intrinsic does not access memory.
    bool maybeHandleSimpleNomemIntrinsic(IntrinsicInst &I) {
        Type *RetTy = I.getType();
        if (!(RetTy->isIntOrIntVectorTy() ||
                RetTy->isFPOrFPVectorTy() ||
                RetTy->isX86_MMXTy()))
            return false;

        unsigned NumArgOperands = I.getNumArgOperands();

        for (unsigned i = 0; i < NumArgOperands; ++i) {
            Type *Ty = I.getArgOperand(i)->getType();
            if (Ty != RetTy)
                return false;
        }

        IRBuilder<> IRB(&I);
        Combiner SC(this, IRB);
        for (unsigned i = 0; i < NumArgOperands; ++i)
            SC.Add(I.getArgOperand(i));
        SC.Done(&I);

        return true;
    }

    /// \brief Heuristically instrument unknown intrinsics.
    ///
    /// The main purpose of this code is to do something reasonable with all
    /// random intrinsics we might encounter, most importantly - SIMD intrinsics.
    /// We recognize several classes of intrinsics by their argument types and
    /// ModRefBehaviour and apply special intrumentation when we are reasonably
    /// sure that we know what the intrinsic does.
    ///
    /// We special-case intrinsics where this approach fails. See llvm.bswap
    /// handling as an example of that.
    bool handleUnknownIntrinsic(IntrinsicInst &I) {
        unsigned NumArgOperands = I.getNumArgOperands();
        if (NumArgOperands == 0)
            return false;

        Intrinsic::ID iid = I.getIntrinsicID();
        IntrinsicKind IK = getIntrinsicKind(iid);
        bool OnlyReadsMemory = IK == IK_OnlyReadsMemory;
        bool WritesMemory = IK == IK_WritesMemory;
        assert(!(OnlyReadsMemory && WritesMemory));

        if (NumArgOperands == 2 &&
                I.getArgOperand(0)->getType()->isPointerTy() &&
                I.getArgOperand(1)->getType()->isVectorTy() &&
                I.getType()->isVoidTy() &&
                WritesMemory) {
            // This looks like a vector store.
            return handleVectorStoreIntrinsic(I);
        }

        if (NumArgOperands == 1 &&
                I.getArgOperand(0)->getType()->isPointerTy() &&
                I.getType()->isVectorTy() &&
                OnlyReadsMemory) {
            // This looks like a vector load.
            return handleVectorLoadIntrinsic(I);
        }

        if (!OnlyReadsMemory && !WritesMemory)
            if (maybeHandleSimpleNomemIntrinsic(I))
                return true;

        // FIXME: detect and handle SSE maskstore/maskload
        return false;
    }

    void handleBswap(IntrinsicInst &I) {
        IRBuilder<> IRB(&I);
        Value *Op = I.getArgOperand(0);
        Type *OpType = Op->getType();
        Function *BswapFunc = Intrinsic::getDeclaration(
                F.getParent(), Intrinsic::bswap, ArrayRef<Type*>(&OpType, 1));
        setShadow(&I, IRB.CreateCall(BswapFunc, getShadow(Op)));
    }

    void visitIntrinsicInst(IntrinsicInst &I) {
        switch (I.getIntrinsicID()) {
        case llvm::Intrinsic::bswap:
            handleBswap(I);
            break;
        default:
            if (!handleUnknownIntrinsic(I))
                    visitInstruction(I);
            break;
        }
    }

    void visitCallSite(CallSite CS) {
        Instruction &I = *CS.getInstruction();
        assert((CS.isCall() || CS.isInvoke()) && "Unknown type of CallSite");

        // Propagation through constructors and operator=
        if (CS.getCalledFunction()->getName().startswith("_ZNSsC1E") ||     // Constructor for std::string
            CS.getCalledFunction()->getName().startswith("_ZNSsaSE") ||     // Operator= for std::string
            CS.getCalledFunction()->getName().equals("_ZNKSs6substrEmm") || // std::string::substr()
            CS.getCalledFunction()->getName().equals("_ZNKSs4copyEPcmm")    // std::string::copy
                )
        {
            IRBuilder<> IRB(&I);
            Combiner SC(this, IRB);
            SC.Add(CS.getArgument(1));
            SC.Done(CS.getArgument(0));
            return;
        }

        // Propagation through operator+
        if (CS.getCalledFunction()->getName().startswith("_ZStplIcSt11char_traitsIcESaIcEESbIT_T0_T1_")) {
            IRBuilder<> IRB(&I);
            Combiner SC(this, IRB);
            SC.Add(CS.getArgument(1));
            SC.Add(CS.getArgument(2));
            SC.Done(CS.getArgument(0));
        }

        // istream
        if (CS.getCalledFunction()->getName().lower().find("istream") != std::string::npos) {
            setShadow(CS.getArgument(1), getTaintedShadow(CS.getArgument(1)));
        }

        unsigned offset = CS.paramHasAttr(1, Attribute::StructRet) ? 1: 0;

        const TaintConfigParser::AddTaintingParam * AddTaintP = ConfigParser.findAddTaintingParam(CS.getCalledFunction()->getName());
        if (AddTaintP != NULL) {
            for (TaintConfigParser::ArgVector::const_iterator it = AddTaintP->Args.begin();
                 it != AddTaintP->Args.end(); ++it) {
                setShadow(CS.getArgument(*it + offset), getTaintedShadow(CS.getArgument(*it + offset)));
            }
            if (AddTaintP->TaintRet) {
                if (offset) {
                    setShadow(CS.getArgument(0), getTaintedShadow(CS.getArgument(0)));
                }
            }
        }

        const TaintConfigParser::SanitizeParam * SanitizeP =
                ConfigParser.findSanitizeParam(CS.getCalledFunction()->getName());
        if (SanitizeP != NULL) {
            for (TaintConfigParser::ArgVector::const_iterator it = SanitizeP->Args.begin();
                 it != SanitizeP->Args.end(); ++it) {
                setShadow(CS.getArgument(*it + offset), getCleanShadow(CS.getArgument(*it + offset)));
            }
            if (SanitizeP->TaintRet) {
                if (offset) {
                    setShadow(CS.getArgument(0), getCleanShadow(CS.getArgument(0)));
                }
            }
        }

        const TaintConfigParser::TaintErrorParam * ErrorP =
                ConfigParser.findTaintErrorParam(CS.getCalledFunction()->getName());
        if (ErrorP != NULL) {
            for (TaintConfigParser::ArgVector::const_iterator it = ErrorP->Args.begin();
                 it != ErrorP->Args.end(); ++it) {
                insertCheck(CS.getArgument(*it + offset), &I);
            }
        }
        const TaintConfigParser::TaintPropagationParam * PropagationP =
                ConfigParser.findTaintPropagationParam(CS.getCalledFunction()->getName());

        if (PropagationP != NULL) {
            IRBuilder<> IRBAfter(&I);
            Combiner SC(this, IRBAfter);
            for (TaintConfigParser::ArgVector::const_iterator it = PropagationP->SrcArgs.begin();
                 it != PropagationP->SrcArgs.end(); ++it) {
                SC.Add(CS.getArgument(*it + offset));
            }
            for (TaintConfigParser::ArgVector::const_iterator it = PropagationP->DstArgs.begin();
                 it != PropagationP->DstArgs.end(); ++it) {
                SC.Done(CS.getArgument(*it + offset));
            }
            if (PropagationP->TaintRet) {
                if (offset) {
                    SC.Done(CS.getArgument(0));
                }
            }
        }
        // Check arguments for concrete test function
        if (CS.getCalledFunction()->getName().equals("_Z7testFunRSs")) {
          insertCheck(CS.getArgument(0), &I);
        }

        if (CS.isCall()) {
            CallInst *Call = cast<CallInst>(&I);
            // For inline asm, do the usual thing: check argument shadow and mark all
            // outputs as clean. Note that any side effects of the inline asm that are
            // not immediately visible in its constraints are not handled.
            if (Call->isInlineAsm()) {
                visitInstruction(I);
                return;
            }

            // Allow only tail calls with the same types, otherwise
            // we may have a false positive: shadow for a non-void RetVal
            // will get propagated to a void RetVal.
            if (Call->isTailCall() && Call->getType() != Call->getParent()->getType())
                Call->setTailCall(false);

            assert(!isa<IntrinsicInst>(&I) && "intrinsics are handled elsewhere");

            // We are going to insert code that relies on the fact that the callee
            // will become a non-readonly function after it is instrumented by us. To
            // prevent this code from being optimized out, mark that function
            // non-readonly in advance.
            if (Function *Func = Call->getCalledFunction()) {
                // Clear out readonly/readnone attributes.
                AttrBuilder B;
                B.addAttribute(Attribute::ReadOnly).addAttribute(Attribute::ReadNone);
                Func->removeAttributes(AttributeSet::FunctionIndex,
                        AttributeSet::get(Func->getContext(),
                                AttributeSet::FunctionIndex, B));
            }
        }
        IRBuilder<> IRB(&I);
        unsigned ArgOffset = 0;
        DEBUG(dbgs() << "  CallSite: " << I << "\n");
        for (CallSite::arg_iterator ArgIt = CS.arg_begin(), End = CS.arg_end();
                ArgIt != End; ++ArgIt) {
            Value *A = *ArgIt;
            unsigned i = ArgIt - CS.arg_begin();
            if (!A->getType()->isSized()) {
                DEBUG(dbgs() << "Arg " << i << " is not sized: " << I << "\n");
                continue;
            }
            unsigned Size = 0;
            Value *Store = 0;
            // Compute the Shadow for arg even if it is ByVal, because
            // in that case getShadow() will copy the actual arg shadow to
            // __taintsan_param_tls.
            Value *ArgShadow = getShadow(A);
            Value *ArgShadowBase = getShadowPtrForArgument(A, IRB, ArgOffset);
            DEBUG(dbgs() << "  Arg#" << i << ": " << *A <<
                    " Shadow: " << *ArgShadow << "\n");
            if (CS.paramHasAttr(i + 1, Attribute::ByVal)) {
                assert(A->getType()->isPointerTy() &&
                        "ByVal argument is not a pointer!");
                Size = TS.TD->getTypeAllocSize(A->getType()->getPointerElementType());
                unsigned Alignment = CS.getParamAlignment(i + 1);
                Store = IRB.CreateMemCpy(ArgShadowBase,
                        getShadowPtr(A, Type::getInt8Ty(*TS.C), IRB),
                        Size, Alignment);
            } else {
                Size = TS.TD->getTypeAllocSize(A->getType());
                Store = IRB.CreateAlignedStore(ArgShadow, ArgShadowBase,
                kShadowTLSAlignment);
            }

            (void)Store;
            assert(Size != 0 && Store != 0);
            DEBUG(dbgs() << "  Param:" << *Store << "\n");
            ArgOffset += DataLayout::RoundUpAlignment(Size, 8);
        }
        DEBUG(dbgs() << "  done with call args\n");

        FunctionType *FT = cast<FunctionType>(CS.getCalledValue()->getType()-> getContainedType(0));
        if (FT->isVarArg()) {
            VAHelper->visitCallSite(CS, IRB);
        }
        // Now, get the shadow for the RetVal.
        if (!I.getType()->isSized()) return;
        IRBuilder<> IRBBefore(&I);
        // Untill we have full dynamic coverage, make sure the retval shadow is 0.
        Value *Base = getShadowPtrForRetval(&I, IRBBefore);
        IRBBefore.CreateAlignedStore(getCleanShadow(&I), Base, kShadowTLSAlignment);
        Instruction *NextInsn = 0;
        if (CS.isCall()) {
            NextInsn = I.getNextNode();
        } else {
            BasicBlock *NormalDest = cast<InvokeInst>(&I)->getNormalDest();
            if (!NormalDest->getSinglePredecessor()) {
                setShadow(&I, getCleanShadow(&I));
                return;
            }
            NextInsn = NormalDest->getFirstInsertionPt();
            assert(NextInsn &&
                    "Could not find insertion point for retval shadow load");
        }
        IRBuilder<> IRBAfter(NextInsn);
        Value *RetvalShadow;
        // check if it was call of std::string methods c_str or data
        if (CS.getCalledFunction()->getName().equals("_ZNKSs5c_strEv") ||
                CS.getCalledFunction()->getName().equals("_ZNKSs4dataEv")) {
            RetvalShadow = getShadow(CS.getArgument(0));
        } else if (AddTaintP != NULL && AddTaintP->TaintRet) {
            RetvalShadow = getTaintedShadow(&I);
        } else if (SanitizeP != NULL && SanitizeP->TaintRet) {
            RetvalShadow = getCleanShadow(&I);
        }else {
            RetvalShadow = IRBAfter.CreateAlignedLoad(getShadowPtrForRetval(&I, IRBAfter),
                        kShadowTLSAlignment, "_tsret");
        }
        setShadow(&I, RetvalShadow);
    }

    void visitReturnInst(ReturnInst &I) {
        IRBuilder<> IRB(&I);
        if (Value *RetVal = I.getReturnValue()) {
            // Set the shadow for the RetVal.
            Value *Shadow = getShadow(RetVal);
            Value *ShadowPtr = getShadowPtrForRetval(RetVal, IRB);
            DEBUG(dbgs() << "Return: " << *Shadow << "\n" << *ShadowPtr << "\n");
            IRB.CreateAlignedStore(Shadow, ShadowPtr, kShadowTLSAlignment);
        }
    }

    void visitPHINode(PHINode &I) {
        IRBuilder<> IRB(&I);
        ShadowPHINodes.push_back(&I);
        setShadow(&I, IRB.CreatePHI(getShadowTy(&I), I.getNumIncomingValues(),
                "_tsphi_s"));
    }

    void visitAllocaInst(AllocaInst &I) {
        setShadow(&I, getCleanShadow(&I));
    }

    void visitSelectInst(SelectInst& I) {
        IRBuilder<> IRB(&I);
        setShadow(&I,  IRB.CreateSelect(I.getCondition(),
                getShadow(I.getTrueValue()), getShadow(I.getFalseValue()),
                "_tsprop"));
    }

    void visitLandingPadInst(LandingPadInst &I) {
        setShadow(&I, getCleanShadow(&I));
    }

    void visitGetElementPtrInst(GetElementPtrInst &I) {
        handleShadowOr(I);
    }

    void visitExtractValueInst(ExtractValueInst &I) {
        IRBuilder<> IRB(&I);
        Value *Agg = I.getAggregateOperand();
        DEBUG(dbgs() << "ExtractValue:  " << I << "\n");
        Value *AggShadow = getShadow(Agg);
        DEBUG(dbgs() << "   AggShadow:  " << *AggShadow << "\n");
        Value *ResShadow = IRB.CreateExtractValue(AggShadow, I.getIndices());
        DEBUG(dbgs() << "   ResShadow:  " << *ResShadow << "\n");
        setShadow(&I, ResShadow);
    }

    void visitInsertValueInst(InsertValueInst &I) {
        IRBuilder<> IRB(&I);
        DEBUG(dbgs() << "InsertValue:  " << I << "\n");
        Value *AggShadow = getShadow(I.getAggregateOperand());
        Value *InsShadow = getShadow(I.getInsertedValueOperand());
        DEBUG(dbgs() << "   AggShadow:  " << *AggShadow << "\n");
        DEBUG(dbgs() << "   InsShadow:  " << *InsShadow << "\n");
        Value *Res = IRB.CreateInsertValue(AggShadow, InsShadow, I.getIndices());
        DEBUG(dbgs() << "   Res:        " << *Res << "\n");
        setShadow(&I, Res);
    }

    void visitResumeInst(ResumeInst &I) {
        DEBUG(dbgs() << "Resume: " << I << "\n");
        // Nothing to do here.
    }

    void visitInstruction(Instruction &I) {
        // Everything else: propagate due operands.
        DEBUG(dbgs() << "DEFAULT: " << I << "\n");
        handleShadowOr(I);
    }
};

/// \brief AMD64-specific implementation of VarArgHelper.
struct VarArgAMD64Helper : public VarArgHelper {
    static const unsigned AMD64GpEndOffset = 48;  // AMD64 ABI Draft 0.99.6 p3.5.7
    static const unsigned AMD64FpEndOffset = 176;

    Function &F;
    TaintSanitizer &TS;
    TaintSanitizerVisitor &TSV;
    Value *VAArgTLSCopy;
    Value *VAArgOverflowSize;

    SmallVector<CallInst*, 16> VAStartInstrumentationList;

    VarArgAMD64Helper(Function &F, TaintSanitizer &TS,
            TaintSanitizerVisitor &TSV):
                F(F), TS(TS), TSV(TSV), VAArgTLSCopy(0), VAArgOverflowSize(0) { }

    enum ArgKind { AK_GeneralPurpose, AK_FloatingPoint, AK_Memory };

    ArgKind classifyArgument(Value* arg) {
        // A very rough approximation of X86_64 argument classification rules.
        Type *T = arg->getType();
        if (T->isFPOrFPVectorTy() || T->isX86_MMXTy())
            return AK_FloatingPoint;
        if (T->isIntegerTy() && T->getPrimitiveSizeInBits() <= 64)
            return AK_GeneralPurpose;
        if (T->isPointerTy())
            return AK_GeneralPurpose;
        return AK_Memory;
    }

    // For VarArg functions, store the argument shadow in an ABI-specific format
    // that corresponds to va_list layout.
    // We do this because Clang lowers va_arg in the frontend, and this pass
    // only sees the low level code that deals with va_list internals.
    // A much easier alternative (provided that Clang emits va_arg instructions)
    // would have been to associate each live instance of va_list with a copy of
    // TaintSanParamTLS, and extract shadow on va_arg() call in the argument list
    // order.
    void visitCallSite(CallSite &CS, IRBuilder<> &IRB) {
        unsigned GpOffset = 0;
        unsigned FpOffset = AMD64GpEndOffset;
        unsigned OverflowOffset = AMD64FpEndOffset;
        for (CallSite::arg_iterator ArgIt = CS.arg_begin(), End = CS.arg_end();
                ArgIt != End; ++ArgIt) {
            Value *A = *ArgIt;
            ArgKind AK = classifyArgument(A);
            if (AK == AK_GeneralPurpose && GpOffset >= AMD64GpEndOffset)
                AK = AK_Memory;
            if (AK == AK_FloatingPoint && FpOffset >= AMD64FpEndOffset)
                AK = AK_Memory;
            Value *Base;
            switch (AK) {
            case AK_GeneralPurpose:
                Base = getShadowPtrForVAArgument(A, IRB, GpOffset);
                GpOffset += 8;
                break;
            case AK_FloatingPoint:
                Base = getShadowPtrForVAArgument(A, IRB, FpOffset);
                FpOffset += 16;
                break;
            case AK_Memory:
                uint64_t ArgSize = TS.TD->getTypeAllocSize(A->getType());
                Base = getShadowPtrForVAArgument(A, IRB, OverflowOffset);
                OverflowOffset += DataLayout::RoundUpAlignment(ArgSize, 8);
            }
            IRB.CreateAlignedStore(TSV.getShadow(A), Base, kShadowTLSAlignment);
        }
        Constant *OverflowSize =
                ConstantInt::get(IRB.getInt64Ty(), OverflowOffset - AMD64FpEndOffset);
        IRB.CreateStore(OverflowSize, TS.VAArgOverflowSizeTLS);
    }

      /// \brief Compute the shadow address for a given va_arg.
    Value *getShadowPtrForVAArgument(Value *A, IRBuilder<> &IRB,
            int ArgOffset) {
        Value *Base = IRB.CreatePointerCast(TS.VAArgTLS, TS.IntptrTy);
        Base = IRB.CreateAdd(Base, ConstantInt::get(TS.IntptrTy, ArgOffset));
        return IRB.CreateIntToPtr(Base, PointerType::get(TSV.getShadowTy(A), 0), "_tsarg");
    }

    void visitVAStartInst(VAStartInst &I) {
        IRBuilder<> IRB(&I);
        VAStartInstrumentationList.push_back(&I);
        Value *VAListTag = I.getArgOperand(0);
        Value *ShadowPtr = TSV.getShadowPtr(VAListTag, IRB.getInt8Ty(), IRB);

        IRB.CreateMemSet(ShadowPtr, Constant::getNullValue(IRB.getInt8Ty()),
                /* size */24, /* alignment */8, false);
    }

    void visitVACopyInst(VACopyInst &I) {
        IRBuilder<> IRB(&I);
        Value *VAListTag = I.getArgOperand(0);
        Value *ShadowPtr = TSV.getShadowPtr(VAListTag, IRB.getInt8Ty(), IRB);


        IRB.CreateMemSet(ShadowPtr, Constant::getNullValue(IRB.getInt8Ty()),
                /* size */24, /* alignment */8, false);
    }

    void finalizeInstrumentation() {
        assert(!VAArgOverflowSize && !VAArgTLSCopy && "finalizeInstrumentation called twice");
        if (!VAStartInstrumentationList.empty()) {
            // If there is a va_start in this function, make a backup copy of
            // va_arg_tls somewhere in the function entry block.
            IRBuilder<> IRB(F.getEntryBlock().getFirstNonPHI());
            VAArgOverflowSize = IRB.CreateLoad(TS.VAArgOverflowSizeTLS);
            Value *CopySize = IRB.CreateAdd(ConstantInt::get(TS.IntptrTy, AMD64FpEndOffset),
                    VAArgOverflowSize);
            VAArgTLSCopy = IRB.CreateAlloca(Type::getInt8Ty(*TS.C), CopySize);
            IRB.CreateMemCpy(VAArgTLSCopy, TS.VAArgTLS, CopySize, 8);
        }

        // Instrument va_start.
        // Copy va_list shadow from the backup copy of the TLS contents.
        for (size_t i = 0, n = VAStartInstrumentationList.size(); i < n; i++) {
            CallInst *OrigInst = VAStartInstrumentationList[i];
            IRBuilder<> IRB(OrigInst->getNextNode());
            Value *VAListTag = OrigInst->getArgOperand(0);

            Value *RegSaveAreaPtrPtr = IRB.CreateIntToPtr(
                    IRB.CreateAdd(IRB.CreatePtrToInt(VAListTag, TS.IntptrTy),
                            ConstantInt::get(TS.IntptrTy, 16)),
                            Type::getInt64PtrTy(*TS.C));
            Value *RegSaveAreaPtr = IRB.CreateLoad(RegSaveAreaPtrPtr);
            Value *RegSaveAreaShadowPtr = TSV.getShadowPtr(RegSaveAreaPtr, IRB.getInt8Ty(), IRB);
            IRB.CreateMemCpy(RegSaveAreaShadowPtr, VAArgTLSCopy, AMD64FpEndOffset, 16);

            Value *OverflowArgAreaPtrPtr = IRB.CreateIntToPtr(
                    IRB.CreateAdd(IRB.CreatePtrToInt(VAListTag, TS.IntptrTy),
                            ConstantInt::get(TS.IntptrTy, 8)),
                            Type::getInt64PtrTy(*TS.C));
            Value *OverflowArgAreaPtr = IRB.CreateLoad(OverflowArgAreaPtrPtr);
            Value *OverflowArgAreaShadowPtr = TSV.getShadowPtr(OverflowArgAreaPtr, IRB.getInt8Ty(), IRB);
            Value *SrcPtr = getShadowPtrForVAArgument(VAArgTLSCopy, IRB, AMD64FpEndOffset);
            IRB.CreateMemCpy(OverflowArgAreaShadowPtr, SrcPtr, VAArgOverflowSize, 16);
        }
    }
};

VarArgHelper* CreateVarArgHelper(Function &Func, TaintSanitizer &Tsan, TaintSanitizerVisitor &Visitor) {
    return new VarArgAMD64Helper(Func, Tsan, Visitor);
}

}  // namespace

bool TaintSanitizer::runOnFunction(Function &F) {
    TaintSanitizerVisitor Visitor(F, *this);

    // Clear out readonly/readnone attributes.
    AttrBuilder B;
    B.addAttribute(Attribute::ReadOnly).addAttribute(Attribute::ReadNone);
    F.removeAttributes(AttributeSet::FunctionIndex,
            AttributeSet::get(F.getContext(), AttributeSet::FunctionIndex, B));

    return Visitor.runOnFunction();
}
