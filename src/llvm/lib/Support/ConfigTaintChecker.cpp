#include "llvm/Support/ConfigTaintChecker.h"

TaintConfigParser::~TaintConfigParser() {
    clearRules();
}

void TaintConfigParser::clearRules() {
    for (std::map<std::string, AddTaintingParam *>::iterator it = AddTaintingRules.begin();
         it != AddTaintingRules.end(); ++it) {
        delete it->second;
    }
    for (std::map<std::string, TaintPropagationParam *>::iterator it = PropagationRules.begin();
         it != PropagationRules.end(); ++it) {
        delete it->second;
    }
    for (std::map<std::string, SanitizeParam *>::iterator it = SanitizeRules.begin();
         it != SanitizeRules.end(); ++it) {
        delete it->second;
    }
    for (std::map<std::string, TaintErrorParam *>::iterator it = TaintErrorRules.begin();
         it != TaintErrorRules.end(); ++it) {
        delete it->second;
    }
    AddTaintingRules.clear();
    PropagationRules.clear();
    SanitizeRules.clear();
    TaintErrorRules.clear();
    EscapeSequencesRules.clear();
}

void TaintConfigParser::parse(std::string Filename) {
    std::ifstream is;
    is.open(Filename.c_str());
    if (!is.is_open()) {
        return;
    }
    clearRules();
    std::vector<std::string> Tokens;
    PropagationRules.clear();
    AddTaintingRules.clear();

    while(nextToken(is, Tokens)) {
        if (!switchRule(Tokens)) {
            std::cerr << "error parsing config file" << std::endl;
            break;
        }
        if (!nextToken(is, Tokens)) break;
        while (Rule == ADD_TAINTING_RULE) {
            std::string Name = getName(Tokens);
            if (Name.empty()) {
                std::cerr << "Parsing rule error" << std::endl;
                break;
            }
            if (!nextToken(is, Tokens)) {
                std::cerr << "Parse AddTaintingRule error" << std::endl;
                break;
            }
            ArgVector Args;
            if (Tokens.size() > 0 && Tokens[0] == "args") {
                Args = getArgs(Tokens, "args");
                if (!nextToken(is, Tokens)) {
                    std::cerr << "Parse AddTaintingRule error" << std::endl;
                    break;
                }
            }
            int TaintRet = getRetVal(Tokens);
            if (TaintRet == -1) {
                break;
            }
            AddTaintingRules[Name] = new AddTaintingParam(Args, TaintRet);
            if (!nextToken(is, Tokens)) {
                Rule = UNKNOWN;
                break;
            }
            if (switchRule(Tokens))
                nextToken(is, Tokens);
        }
        while(Rule == PROPAGATION_RULE) {
            std::string Name = getName(Tokens);
            if (Name.empty()) {
                std::cerr << "Parsing rule error" << std::endl;
                break;
            }
            if (!nextToken(is, Tokens)) {
                std::cerr << "Parse PropagatonRule error" << std::endl;
                break;
            }
            ArgVector Src = getArgs(Tokens, "src_args");
            if (!nextToken(is, Tokens)) {
                std::cerr << "Parse PropagatonRule error" << std::endl;
                break;
            }
            ArgVector Dst = getArgs(Tokens, "dst_args");
            if (!nextToken(is, Tokens)) {
                std::cerr << "Parse PropagatonRule error" << std::endl;
                break;
            }
            int TaintRet = getRetVal(Tokens);
            if (TaintRet == -1) {
                break;
            }
            PropagationRules[Name] = new TaintPropagationParam(Src, Dst, TaintRet);
            if (!nextToken(is, Tokens)) {
                Rule = UNKNOWN;
                break;
            }
            if (switchRule(Tokens))
                nextToken(is, Tokens);
        }
        while(Rule == UNTAINTING_RULE) {
            std::string Name = getName(Tokens);
            if (Name.empty()) {
                std::cerr << "Parsing rule error" << std::endl;
                break;
            }
            if (!nextToken(is, Tokens)) {
                std::cerr << "Parse SanitizeRule error" << std::endl;
                break;
            }
            ArgVector Args = getArgs(Tokens, "args");
            if (!nextToken(is, Tokens)) {
                std::cerr << "Parse SanitizeRule error" << std::endl;
                break;
            }
            int TaintRet = getRetVal(Tokens);
            if (TaintRet == -1) {
                break;
            }
            SanitizeRules[Name] = new SanitizeParam(Args, TaintRet);
            if (!nextToken(is, Tokens)) {
                Rule = UNKNOWN;
                break;
            }
            if (switchRule(Tokens))
                nextToken(is, Tokens);
        }
        while(Rule == TAINT_ERROR_RULE) {
            std::string Name = getName(Tokens);
            if (Name.empty()) {
                std::cerr << "Parsing rule error" << std::endl;
                break;
            }
            if (!nextToken(is, Tokens)) {
                std::cerr << "Parse TainErrorRule error" << std::endl;
                break;
            }
            ArgVector Args = getArgs(Tokens, "args");
            TaintErrorRules[Name] = new TaintErrorParam(Args);
            if (!nextToken(is, Tokens)) {
                Rule = UNKNOWN;
                break;
            }
            if (switchRule(Tokens))
                nextToken(is, Tokens);
        }
        while (Rule == ESCAPE_SEQUENCE_RULE) {
            EscapeSequencesRule rule = getEscapeSequenceRule(Tokens);
            if (!rule.first.empty()) {
                EscapeSequencesRules.insert(rule);
            }
            if (!nextToken(is, Tokens)) {
                Rule = UNKNOWN;
                break;
            }
            if (switchRule(Tokens))
                nextToken(is, Tokens);
        }
        Rule = UNKNOWN;
    }
}

TaintConfigParser::AddTaintingParam const *TaintConfigParser::getAddTaintingParam(std::string const& Name) const{
     std::map<std::string, AddTaintingParam *>::const_iterator it = AddTaintingRules.find(Name);
     if (it != AddTaintingRules.end()) {
         return it->second;
     }
     return NULL;
}

TaintConfigParser::TaintPropagationParam const* TaintConfigParser::getTaintPropagationParam(std::string const& Name) const {
    std::map<std::string, TaintPropagationParam *>::const_iterator it = PropagationRules.find(Name);
    if (it != PropagationRules.end()) {
        return it->second;
    }
    return NULL;
}

TaintConfigParser::SanitizeParam const* TaintConfigParser::getSanitizeParam(std::string const& Name) const {
    std::map<std::string, SanitizeParam *>::const_iterator it = SanitizeRules.find(Name);
    if (it != SanitizeRules.end()) {
        return it->second;
    }
    return NULL;
}

TaintConfigParser::TaintErrorParam const* TaintConfigParser::getTaintErrorParam(std::string const& Name) const {
    std::map<std::string, TaintErrorParam *>::const_iterator it = TaintErrorRules.find(Name);
    if (it != TaintErrorRules.end()) {
        return it->second;
    }
    return NULL;
}

bool TaintConfigParser::nextToken(std::ifstream &is, std::vector<std::string> &Tokens) {
    std::string Str;
    Tokens.clear();
    while(std::getline(is, Str)) {
        if (Str.empty()) {
            continue;
        }
        split(Str, " =,#", Tokens);
        if (!Tokens.empty()) return true;
    }
    return false;
}

const std::map<std::string, std::string> &TaintConfigParser::getReplaceSequences() const {
    return EscapeSequencesRules;
}

void TaintConfigParser::split(std::string const& Str, std::string const& Delim, std::vector<std::string>& Tokens) {
    size_t Start, End = 0;
    std::string TempStr = Str;
    while (End < TempStr.size()) {
        Start = End;
        while (Start < TempStr.size() && (Delim.find(TempStr[Start]) != std::string::npos)) {
            if (TempStr[Start] == '=') {
                Tokens.push_back("=");
            }
            if (TempStr[Start] == '#')
                return;
            ++Start;
        }

        End = Start;
        std::string Token;
        while (End < TempStr.size() && (Delim.find(Str[End]) == std::string::npos)) {
            ++End;
        }
        Token = std::string(TempStr, Start, End-Start);

        if (End-Start != 0) {
            Tokens.push_back(Token);
        }
    }
}

bool TaintConfigParser::switchRule(const std::vector<std::string> &Tokens) {
    if (Tokens.size() != 1) {
        return false;
    }
    if (Tokens[0] == "[AddTaintingRule]") {
        Rule = ADD_TAINTING_RULE;
        return true;
    }else if (Tokens[0] == "[TaintPropagationRules]") {
        Rule = PROPAGATION_RULE;
        return true;
    } else if (Tokens[0] == "[UntaintingRules]") {
        Rule = UNTAINTING_RULE;
        return true;
    } else if (Tokens[0] == "[TaintingErrorRules]") {
        Rule = TAINT_ERROR_RULE;
        return true;
    } else if (Tokens[0] == "[EscapeSequencesRules]") {
        Rule = ESCAPE_SEQUENCE_RULE;
        return true;
    }

    return false;

}

std::string TaintConfigParser::getName(std::vector<std::string> const& Tokens) {
    if (Tokens.size() < 3 || Tokens[0] != "name" || Tokens[1] != "=") {
        std::cerr << "Parse function name error" << std::endl;
        return "";
    }
    return Tokens[2];
}

TaintConfigParser::ArgVector TaintConfigParser::getArgs(const std::vector<std::string> &Tokens, const std::string &ArgName) {
    if (Tokens.size() < 3 || Tokens[0] != ArgName || Tokens[1] != "=") {
        std::cerr << "Parse args error" << std::endl;
        return ArgVector();
    }
    ArgVector Result;
    for (unsigned i = 2; i < Tokens.size(); ++i) {
        unsigned val = atoi(Tokens[i].c_str());
        if (val == 0 && Tokens[i] != "0") std::cerr << "Parse args error" << std::endl;
        Result.push_back(val);
    }
    return Result;
}

int TaintConfigParser::getRetVal(const std::vector<std::string> &Tokens) {
    if (Tokens.size() < 3 || Tokens[0] != "ret_val" || Tokens[1] != "=") {
        std::cerr << "Parse ret_val error" << std::endl;
        return -1;
    }
    if (Tokens[2] == "true") return 1;
    else if (Tokens[2] == "false") return 0;
    else std::cerr << "Parse ret_val error" << std::endl;
    return 1;
}

TaintConfigParser::EscapeSequencesRule TaintConfigParser::getEscapeSequenceRule(const std::vector<std::string> &Tokens) {
    EscapeSequencesRule rule;
    if (Tokens.size() < 3 || Tokens[1] != "=") {
        std::cerr << "Parse EscapeSequenceRule error" << std::endl;
        return rule;
    }

    rule.first = Tokens[0];
    rule.second = Tokens[2];
    return rule;
}

// Methods for taintSanititizer
TaintConfigParser::AddTaintingParam const *TaintConfigParser::findAddTaintingParam(std::string const& Name) const{
    for (std::map<std::string, AddTaintingParam *>::const_iterator it = AddTaintingRules.begin();
         it != AddTaintingRules.end(); ++it) {
        if (it->first.find(Name) != std::string::npos) {
            return it->second;
        }
    }
    return NULL;
}

TaintConfigParser::TaintPropagationParam const* TaintConfigParser::findTaintPropagationParam(std::string const& Name) const {
    for (std::map<std::string, TaintPropagationParam *>::const_iterator it = PropagationRules.begin();
         it != PropagationRules.end(); ++it) {
        if (it->first.find(Name) != std::string::npos) {
            return it->second;
        }
    }
    return NULL;
}

TaintConfigParser::SanitizeParam const* TaintConfigParser::findSanitizeParam(std::string const& Name) const {
    for (std::map<std::string, SanitizeParam *>::const_iterator it = SanitizeRules.begin();
         it != SanitizeRules.end(); ++it) {
        if (it->first.find(Name) != std::string::npos) {
            return it->second;
        }
    }
    return NULL;
}

TaintConfigParser::TaintErrorParam const* TaintConfigParser::findTaintErrorParam(std::string const& Name) const {
    for (std::map<std::string, TaintErrorParam *>::const_iterator it = TaintErrorRules.begin();
         it != TaintErrorRules.end(); ++it) {
        if (it->first.find(Name) != std::string::npos) {
            return it->second;
        }
    }
    return NULL;
}