#include "ClangSACheckers.h"
#include "clang/AST/Attr.h"
#include "clang/Basic/Builtins.h"
#include "clang/StaticAnalyzer/Core/BugReporter/BugType.h"
#include "clang/StaticAnalyzer/Core/Checker.h"
#include "clang/StaticAnalyzer/Core/CheckerManager.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ProgramStateTrait.h"
#include "llvm/Support/ConfigTaintChecker.h"
#include <climits>
#include <iostream>
#include <sstream>

using namespace clang;
using namespace ento;

#define REDECLARABLE_OP(type, op) \
    if(clang::Redeclarable<type> const* decl_ = dyn_cast_or_null<clang::Redeclarable<type>>(decl)) \
    return decl_->op();

#define GET_FIRST_DECL(type) REDECLARABLE_OP(type, getFirstDeclaration)

namespace {
class TaintChecker : public Checker< check::PostStmt<CXXMemberCallExpr>,
        check::PreStmt<DeclStmt>,
        check::PostStmt<ImplicitCastExpr>,
        check::PostStmt<CXXOperatorCallExpr>,
        check::PostStmt<CallExpr>,
        check::PreStmt<CallExpr>,
        check::EndAnalysis> {
public:
    TaintChecker() {
        DeclRefExprsMap = new std::map<SourceLocation, DeclRefExpr const *>();
        TaintErrorCalls = new std::map<StringRef, std::vector<const Expr *> *>();
        ConfigParser.parse("rules.cfg");
    }

    ~TaintChecker() {
        delete DeclRefExprsMap;
        for (std::map<StringRef, std::vector<const Expr *> *>::const_iterator it = TaintErrorCalls->begin();
             it != TaintErrorCalls->end(); ++it) {
            delete it->second;
        }
        delete TaintErrorCalls;
    }

    static void *getTag() { static int Tag; return &Tag; }

    void checkPostStmt(const CallExpr *CE, CheckerContext &C) const;
    void checkPostStmt(const ImplicitCastExpr *ISE, CheckerContext &C) const;
    void checkPreStmt(const DeclStmt *DS, CheckerContext &C) const;
    void checkPostStmt(const CXXOperatorCallExpr *CE, CheckerContext &C) const;
    void checkPostStmt(const CXXMemberCallExpr *CE, CheckerContext &C) const;
    void checkPreStmt(const CallExpr *CE, CheckerContext &C) const;
    void checkEndAnalysis(ExplodedGraph &G, BugReporter &B,
                          ExprEngine &Eng) const;
private:
    std::map<SourceLocation, DeclRefExpr const *> * DeclRefExprsMap;
    std::map<StringRef, std::vector<const Expr *> *> * TaintErrorCalls;
    TaintConfigParser ConfigParser;

    bool checkExprIsTainted(const Expr * E, CheckerContext &C) const {
        if (!E) return false;
        ProgramStateRef State = C.getState();
        if (State && (State->isTainted(getPointedToSymbol(C, E)) ||
                      State->isTainted(E, C.getLocationContext())) ) {
            return true;
        }
        if (const DeclRefExpr * DeclRefExpression = getDeclExpr(E)) {
            if (findInTaintDecls(DeclRefExpression, C)) {
                return true;
            }
        }
        return false;
    }

    DeclRefExpr const * getDeclExpr (const Expr * expr) const {
        std::map<SourceLocation, DeclRefExpr const *>::const_iterator it = DeclRefExprsMap->find(expr->getExprLoc());
        if (it != DeclRefExprsMap->end()) {
            return it->second;
        }
        return dyn_cast_or_null<DeclRefExpr>(expr);
    }

    Decl const* getFirstDeclaration(Decl const* decl) const{
        //TODO: find first declaration
        //if(clang::Redeclarable<VarDecl> const* decl_ = dynamic_cast<clang::Redeclarable<VarDecl> const* >(decl))
        //   return decl_->getFirstDeclaration();

        return decl;
    }

    bool findInTaintDecls(const DeclRefExpr* expr, CheckerContext &C) const;
    void generateSanitizeFunctionFile() const;
    unsigned getExpressionLength(const Expr *) const;

    static const unsigned InvalidArgIndex = UINT_MAX;
    /// Denotes the return vale.
    static const unsigned ReturnValueIndex = UINT_MAX - 1;

    mutable OwningPtr<BugType> BT;
    inline void initBugType() const {
        if (!BT)
            BT.reset(new BugType("Use of Untrusted Data", "Untrusted Data"));
    }

    /// \brief Catch taint related bugs. Check if tainted data is passed to a
    /// system call etc.
    bool checkPre(const CallExpr *CE, CheckerContext &C) const;

    /// \brief Add taint sources on a pre-visit.
    void addSourcesPre(const CallExpr *CE, CheckerContext &C) const;

    /// \brief Propagate taint generated at pre-visit.
    bool propagateFromPre(const CallExpr *CE, CheckerContext &C) const;

    /// \brief Add taint sources on a post visit.
    void addSourcesPost(const CallExpr *CE, CheckerContext &C) const;

    void sanitize(const CallExpr *CE, CheckerContext &C) const;

    /// Check if the region the expression evaluates to is the standard input,
    /// and thus, is tainted.
    static bool isStdin(const Expr *E, CheckerContext &C);

    /// \brief Given a pointer argument, get the symbol of the value it contains
    /// (points to).
    static SymbolRef getPointedToSymbol(CheckerContext &C, const Expr *Arg);

    /// Functions defining the attack surface.
    typedef ProgramStateRef (TaintChecker::*FnCheck)(const CallExpr *,
                                                     CheckerContext &C) const;
    ProgramStateRef postScanf(const CallExpr *CE, CheckerContext &C) const;
    ProgramStateRef postSocket(const CallExpr *CE, CheckerContext &C) const;
    ProgramStateRef postRetTaint(const CallExpr *CE, CheckerContext &C) const;

    /// Taint the scanned input if the file is tainted.
    ProgramStateRef preFscanf(const CallExpr *CE, CheckerContext &C) const;

    /// Check for CWE-134: Uncontrolled Format String.
    static const char MsgUncontrolledFormatString[];
    bool checkUncontrolledFormatString(const CallExpr *CE,
                                       CheckerContext &C) const;

    /// Check for:
    /// CERT/STR02-C. "Sanitize data passed to complex subsystems"
    /// CWE-78, "Failure to Sanitize Data into an OS Command"
    static const char MsgSanitizeSystemArgs[];
    bool checkSystemCall(const CallExpr *CE, StringRef Name,
                         CheckerContext &C) const;

    /// Check for:
    /// Special user rules from rules.cfg
    bool checkUserRule(const CallExpr *CE, StringRef Name,
                       CheckerContext &C) const;

    /// Check if tainted data is used as a buffer size ins strn.. functions,
    /// and allocators.
    static const char MsgTaintedBufferSize[];
    bool checkTaintedBufferSize(const CallExpr *CE, const FunctionDecl *FDecl,
                                CheckerContext &C) const;

    /// Generate a report if the expression is tainted or points to tainted data.
    bool generateReportIfTainted(const Expr *E, const char Msg[],
                                 CheckerContext &C) const;


    typedef SmallVector<unsigned, 2> ArgVector;

    /// \brief A struct used to specify taint propagation rules for a function.
    ///
    /// If any of the possible taint source arguments is tainted, all of the
    /// destination arguments should also be tainted. Use InvalidArgIndex in the
    /// src list to specify that all of the arguments can introduce taint. Use
    /// InvalidArgIndex in the dst arguments to signify that all the non-const
    /// pointer and reference arguments might be tainted on return. If
    /// ReturnValueIndex is added to the dst list, the return value will be
    /// tainted.
    struct TaintPropagationRule {
        /// List of arguments which can be taint sources and should be checked.
        ArgVector SrcArgs;
        /// List of arguments which should be tainted on function return.
        ArgVector DstArgs;
        // TODO: Check if using other data structures would be more optimal.

        TaintPropagationRule() {}

        TaintPropagationRule(unsigned SArg,
                             unsigned DArg, bool TaintRet = false) {
            SrcArgs.push_back(SArg);
            DstArgs.push_back(DArg);
            if (TaintRet)
                DstArgs.push_back(ReturnValueIndex);
        }

        TaintPropagationRule(unsigned SArg1, unsigned SArg2,
                             unsigned DArg, bool TaintRet = false) {
            SrcArgs.push_back(SArg1);
            SrcArgs.push_back(SArg2);
            DstArgs.push_back(DArg);
            if (TaintRet)
                DstArgs.push_back(ReturnValueIndex);
        }

        TaintPropagationRule(ArgVector SArgs, ArgVector DArgs, bool TaintRet = false):
            SrcArgs(SArgs), DstArgs(DArgs) {
            if (TaintRet)
                DstArgs.push_back(ReturnValueIndex);
        }

        /// Get the propagation rule for a given function.
        static TaintPropagationRule
        getTaintPropagationRule(const FunctionDecl *FDecl,
                                StringRef Name,
                                CheckerContext &C,
                                const TaintChecker *Checker);

        inline void addSrcArg(unsigned A) { SrcArgs.push_back(A); }
        inline void addDstArg(unsigned A)  { DstArgs.push_back(A); }

        inline bool isNull() const { return SrcArgs.empty(); }

        inline bool isDestinationArgument(unsigned ArgNum) const {
            return (std::find(DstArgs.begin(),
                              DstArgs.end(), ArgNum) != DstArgs.end());
        }

        static inline bool isTaintedOrPointsToTainted(const Expr *E,
                                                      ProgramStateRef State,
                                                      CheckerContext &C,
                                                      const TaintChecker *Checker) {
            return (Checker->checkExprIsTainted(E, C) || isStdin(E, C));
        }

        /// \brief Pre-process a function which propagates taint according to the
        /// taint rule.
        ProgramStateRef process(const CallExpr *CE, CheckerContext &C, const TaintChecker *Checker) const;

    };

    struct AddTaintingRule {
        /// List of arguments which should be tainted on function return.
        ArgVector DstArgs;

        AddTaintingRule() {}
        AddTaintingRule(ArgVector DArgs, bool TaintRet = false):DstArgs(DArgs) {
            if (TaintRet)
                DstArgs.push_back(ReturnValueIndex);
        }
        /// Get the add tainting rule for a given function.
        static AddTaintingRule
        getAddTaintingRule(const FunctionDecl *FDecl,
                           StringRef Name,
                           CheckerContext &C,
                           const TaintChecker *Checker);
        inline void addDstArg(unsigned A)  { DstArgs.push_back(A); }
        inline bool isNull() const { return DstArgs.empty(); }
        ProgramStateRef process(const CallExpr *CE, CheckerContext &C, const TaintChecker *Checker) const; //TODO

    };

    struct SanitizeRule {
        /// List of arguments which should be untainted on function return.
        ArgVector DstArgs;

        SanitizeRule() {}
        SanitizeRule(ArgVector DArgs, bool TaintRet = false): DstArgs(DArgs) {
            if (TaintRet)
                DstArgs.push_back(ReturnValueIndex);
        }
        /// Get the sanitize rule for a given function.
        static SanitizeRule
        getSanitizeRule(StringRef Name,
                        const TaintChecker *Checker);
        inline void addDstArg(unsigned A)  { DstArgs.push_back(A); }
        inline bool isNull() const { return DstArgs.empty(); }
        ProgramStateRef process(const CallExpr *CE, CheckerContext &C, const TaintChecker *Checker) const;

    };

    struct TaintingErrorRule {
        /// List of arguments which can be taint sources and should be checked.
        ArgVector SrcArgs;
        TaintingErrorRule() {}
        TaintingErrorRule(ArgVector SArgs):
            SrcArgs(SArgs){}

        /// Get the tainting error rule for a given function.
        static TaintingErrorRule
        getTaintingErrorRule(StringRef Name,
                             const TaintChecker *Checker);

        inline void addSrcArg(unsigned A) { SrcArgs.push_back(A); }
        inline bool isNull() const { return SrcArgs.empty(); }
        static inline bool isTaintedOrPointsToTainted(const Expr *E,
                                                      ProgramStateRef State,
                                                      CheckerContext &C,
                                                      const TaintChecker *Checker) {
            return (Checker->checkExprIsTainted(E, C) || isStdin(E, C));
        }
        ProgramStateRef process(const CallExpr *CE, CheckerContext &C, const TaintChecker *Checker) const; //TODO

    };
};

const unsigned TaintChecker::ReturnValueIndex;
const unsigned TaintChecker::InvalidArgIndex;

const char TaintChecker::MsgUncontrolledFormatString[] =
        "Untrusted data is used as a format string "
        "(CWE-134: Uncontrolled Format String)";

const char TaintChecker::MsgSanitizeSystemArgs[] =
        "Untrusted data is passed to a system call "
        "(CERT/STR02-C. Sanitize data passed to complex subsystems)";

const char TaintChecker::MsgTaintedBufferSize[] =
        "Untrusted data is used to specify the buffer size "
        "(CERT/STR31-C. Guarantee that storage for strings has sufficient space for "
        "character data and the null terminator)";

} // end of anonymous namespace

/// A set which is used to pass information from call pre-visit instruction
/// to the call post-visit. The values are unsigned integers, which are either
/// ReturnValueIndex, or indexes of the pointer/reference argument, which
/// points to data, which should be tainted on return.
REGISTER_SET_WITH_PROGRAMSTATE(TaintArgsOnPostVisit, unsigned)
REGISTER_SET_WITH_PROGRAMSTATE(TaintDecls, Decl const *)

TaintChecker::TaintPropagationRule
TaintChecker::TaintPropagationRule::getTaintPropagationRule(
        const FunctionDecl *FDecl,
        StringRef Name,
        CheckerContext &C,
        const TaintChecker *Checker) {
    // TODO: Currently, we might loose precision here: we always mark a return
    // value as tainted even if it's just a pointer, pointing to tainted data.

    // Check for exact name match for functions without builtin substitutes.
    TaintPropagationRule Rule = llvm::StringSwitch<TaintPropagationRule>(Name)
            .Case("atoi", TaintPropagationRule(0, ReturnValueIndex))
            .Case("atol", TaintPropagationRule(0, ReturnValueIndex))
            .Case("atoll", TaintPropagationRule(0, ReturnValueIndex))
            .Case("getc", TaintPropagationRule(0, ReturnValueIndex))
            .Case("fgetc", TaintPropagationRule(0, ReturnValueIndex))
            .Case("getc_unlocked", TaintPropagationRule(0, ReturnValueIndex))
            .Case("getw", TaintPropagationRule(0, ReturnValueIndex))
            .Case("toupper", TaintPropagationRule(0, ReturnValueIndex))
            .Case("tolower", TaintPropagationRule(0, ReturnValueIndex))
            .Case("strchr", TaintPropagationRule(0, ReturnValueIndex))
            .Case("strrchr", TaintPropagationRule(0, ReturnValueIndex))
            .Case("read", TaintPropagationRule(0, 2, 1, true))
            .Case("pread", TaintPropagationRule(InvalidArgIndex, 1, true))
            .Case("gets", TaintPropagationRule(InvalidArgIndex, 0, true))
            .Case("fgets", TaintPropagationRule(2, 0, true))
            .Case("getline", TaintPropagationRule(2, 0))
            .Case("getdelim", TaintPropagationRule(3, 0))
            .Case("fgetln", TaintPropagationRule(0, ReturnValueIndex))
            .Default(TaintPropagationRule());

    if (!Rule.isNull()) {
        return Rule;
    }

    // Check if it's one of the memory setting/copying functions.
    // This check is specialized but faster then calling isCLibraryFunction.
    unsigned BId = 0;
    if ( (BId = FDecl->getMemoryFunctionKind()) )
        switch(BId) {
        case Builtin::BImemcpy:
        case Builtin::BImemmove:
        case Builtin::BIstrncpy:
        case Builtin::BIstrncat:
            return TaintPropagationRule(1, 2, 0, true);
        case Builtin::BIstrlcpy:
        case Builtin::BIstrlcat:
            return TaintPropagationRule(1, 2, 0, false);
        case Builtin::BIstrndup:
            return TaintPropagationRule(0, 1, ReturnValueIndex);

        default:
            break;
        };

    // Process all other functions which could be defined as builtins.
    if (Rule.isNull()) {
        if (C.isCLibraryFunction(FDecl, "snprintf") ||
                C.isCLibraryFunction(FDecl, "sprintf"))
            return TaintPropagationRule(InvalidArgIndex, 0, true);
        else if (C.isCLibraryFunction(FDecl, "strcpy") ||
                 C.isCLibraryFunction(FDecl, "stpcpy") ||
                 C.isCLibraryFunction(FDecl, "strcat"))
            return TaintPropagationRule(1, 0, true);
        else if (C.isCLibraryFunction(FDecl, "bcopy"))
            return TaintPropagationRule(0, 2, 1, false);
        else if (C.isCLibraryFunction(FDecl, "strdup") ||
                 C.isCLibraryFunction(FDecl, "strdupa"))
            return TaintPropagationRule(0, ReturnValueIndex);
        else if (C.isCLibraryFunction(FDecl, "wcsdup"))
            return TaintPropagationRule(0, ReturnValueIndex);
    }

    // Skipping the following functions, since they might be used for cleansing
    // or smart memory copy:
    // - memccpy - copying until hitting a special character.

    const TaintConfigParser::TaintPropagationParam * param = Checker->ConfigParser.getTaintPropagationParam(Name);
    if (param != NULL) {
        return TaintPropagationRule(param->SrcArgs, param->DstArgs, param->TaintRet);
    }

    return TaintPropagationRule();
}

TaintChecker::AddTaintingRule
TaintChecker::AddTaintingRule::getAddTaintingRule(const FunctionDecl *FDecl, StringRef Name, CheckerContext &C,
                                                  const TaintChecker *Checker) {
    const TaintConfigParser::AddTaintingParam * param = Checker->ConfigParser.getAddTaintingParam(Name);
    if (param != NULL) {
        return AddTaintingRule(param->Args, param->TaintRet);
    }
    return AddTaintingRule();
}

TaintChecker::SanitizeRule
TaintChecker::SanitizeRule::getSanitizeRule(StringRef Name, const TaintChecker *Checker) {
    const TaintConfigParser::SanitizeParam * param = Checker->ConfigParser.getSanitizeParam(Name);
    if (param != NULL) {
        return SanitizeRule(param->Args, param->TaintRet);
    }
    return SanitizeRule();
}

TaintChecker::TaintingErrorRule
TaintChecker::TaintingErrorRule::getTaintingErrorRule(StringRef Name, const TaintChecker *Checker) {
    const TaintConfigParser::TaintErrorParam * param = Checker->ConfigParser.getTaintErrorParam(Name);
    if (param != NULL) {
        return TaintingErrorRule(param->Args);
    }
    return TaintingErrorRule();
}


ProgramStateRef TaintChecker::AddTaintingRule::process(const CallExpr *CE, CheckerContext &C, const TaintChecker *Checker) const {
    ProgramStateRef State = C.getState();

    // Mark the arguments which should be tainted after the function returns.
    for (ArgVector::const_iterator I = DstArgs.begin(),
         E = DstArgs.end(); I != E; ++I) {
        unsigned ArgNum = *I;
        // Mark return value
        if (ArgNum == ReturnValueIndex) {
            State = State->addTaint(CE, C.getLocationContext());
            if (const DeclRefExpr *declRef = Checker->getDeclExpr(CE)) {
                Decl const *decl = declRef->getFoundDecl();
                if (decl) {
                    State = State->add<TaintDecls>(decl);
                }
            }
            continue;
        }

        // Mark the given argument.
        if (ArgNum >= CE->getNumArgs()) continue;
        DeclRefExpr const * DE = Checker->getDeclExpr(CE->getArg(ArgNum));
        if (DE) {
            Decl const *decl = DE->getFoundDecl();
            if(decl) {
                State = State->add<TaintDecls>(decl);
            }
        }

        const Expr* Arg = CE->getArg(ArgNum);
        SymbolRef Sym = getPointedToSymbol(C, Arg);
        if (Sym)
            State = State->addTaint(Sym);
    }

    return State;
}

ProgramStateRef TaintChecker::SanitizeRule::process(const CallExpr *CE, CheckerContext &C, const TaintChecker *Checker) const {
    ProgramStateRef State = C.getState();
    for (ArgVector::const_iterator I = DstArgs.begin(),
         E = DstArgs.end(); I != E; ++I) {
        unsigned ArgNum = *I;

        // Unmark return value
        if (ArgNum == ReturnValueIndex) {
            State = State->removeTaint(CE, C.getLocationContext());
            continue;
        }

        // Unmark the given argument.
        assert(ArgNum < CE->getNumArgs());
        const Expr* Arg = CE->getArg(ArgNum);
        Decl const* decl = Checker->getDeclExpr(Arg)->getFoundDecl();
        if(decl) {
            State = State->remove<TaintDecls>(decl);
        }
        SymbolRef Sym = getPointedToSymbol(C, Arg);
        if (Sym)
            State = State->removeTaint(Sym);
    }

    return State;
}

ProgramStateRef TaintChecker::TaintingErrorRule::process(const CallExpr *CE, CheckerContext &C, const TaintChecker *Checker) const {
    ProgramStateRef State = C.getState();
    // Check for taint in arguments.
    for (ArgVector::const_iterator I = SrcArgs.begin(),
         E = SrcArgs.end(); I != E; ++I) {
        unsigned ArgNum = *I;
        if (ArgNum < CE->getNumArgs()) {
            if (Checker->generateReportIfTainted(CE->getArg(ArgNum), TaintChecker::MsgSanitizeSystemArgs, C)) return State;
        }
    }
    return 0;
}

bool TaintChecker::findInTaintDecls(const DeclRefExpr* expr, CheckerContext &C) const {
    clang::Decl const* decl = expr->getFoundDecl();
    if(!decl) return false;
    ProgramStateRef State = C.getState();
    if (!State) return false;
    TaintDeclsTy TaintDeclsSet = State->get<TaintDecls>();
    return TaintDeclsSet.contains(decl);
}

void TaintChecker::checkPreStmt(const CallExpr *CE,
                                CheckerContext &C) const {
    const FunctionDecl *FDecl = C.getCalleeDecl(CE);
    if (!FDecl || FDecl->getKind() != Decl::Function)
        return;

    StringRef Name = C.getCalleeName(FDecl);
    if (!Name.empty())
        // Check for errors first.
        if (checkPre(CE, C))
            return;

    // Add taint second.
    addSourcesPre(CE, C);
}

void TaintChecker::checkPostStmt(const CallExpr *CE,
                                 CheckerContext &C) const {
    const FunctionDecl *FDecl = C.getCalleeDecl(CE);
    if (!FDecl || FDecl->getKind() != Decl::Function)
        return;

    if (propagateFromPre(CE, C)) {
        return;
    }
    addSourcesPost(CE, C);
    sanitize(CE, C);
}

void TaintChecker::checkPostStmt(const ImplicitCastExpr *ISE, CheckerContext &C) const {
    if (DeclRefExpr const * DE = dyn_cast_or_null<DeclRefExpr>(ISE->getSubExpr())) {
        DeclRefExprsMap->insert(std::make_pair(DE->getExprLoc(), DE));
    }
}

void TaintChecker::checkPreStmt(const DeclStmt *DS, CheckerContext &C) const {

    for (DeclStmt::const_decl_iterator DI = DS->decl_begin(), DE = DS->decl_end();
         DI != DE; ++DI) {
        const VarDecl *V = cast<VarDecl>(*DI);
        const Expr *ExprInit = V->getInit();
        // Skip expressions with cleanups from the initializer expression.
        if (!ExprInit) continue;
        if (const ExprWithCleanups *E = dyn_cast<ExprWithCleanups>(ExprInit))
            ExprInit = E->getSubExpr();

        // initializer is function
        if (const CallExpr *CE = dyn_cast_or_null<CallExpr>(ExprInit)) {
            if (checkExprIsTainted(CE, C)) {
                ProgramStateRef State = C.getState();
                State = State->add<TaintDecls>(V);
                if (State) {
                    C.addTransition(State);
                }
                return;
            }
        }
        // initializer is constructor
        else if (const CXXConstructExpr *Construct = dyn_cast_or_null<CXXConstructExpr>(ExprInit)) {
            if (checkExprIsTainted(Construct, C)) {
                ProgramStateRef State = C.getState();
                State = State->add<TaintDecls>(V);
                if (State) {
                    C.addTransition(State);
                }
                return;
            }
            for (size_t i = 0; i < Construct->getNumArgs(); ++i) {
                const Expr * Arg = Construct->getArg(i);
                if (checkExprIsTainted(Arg, C)) {
                    ProgramStateRef State = C.getState();
                    State = State->add<TaintDecls>(V);
                    if (State) {
                        C.addTransition(State);
                    }
                    return;
                }

            }
        }
        // initializer is binary operator
        else if (const BinaryOperator *BinaryOp = dyn_cast_or_null<BinaryOperator>(ExprInit)) {
            if (checkExprIsTainted(BinaryOp->getLHS(), C) || checkExprIsTainted(BinaryOp->getRHS(), C)) {
                ProgramStateRef State = C.getState();
                State = State->add<TaintDecls>(V);
                if (State) {
                    C.addTransition(State);
                }
                return;
            }
        }
        // initializer is unary operator
        else if (const UnaryOperator *UnaryOp = dyn_cast_or_null<UnaryOperator>(ExprInit)) {
            if (checkExprIsTainted(UnaryOp->getSubExpr(), C)) {
                ProgramStateRef State = C.getState();
                State = State->add<TaintDecls>(V);
                if (State) {
                    C.addTransition(State);
                }
                return;
            }
        }
    }
}

void TaintChecker::checkPostStmt(const CXXOperatorCallExpr *CE, CheckerContext &C) const {
    ProgramStateRef State = C.getState();
    const FunctionDecl *FDecl = C.getCalleeDecl(CE);
    if (FDecl && FDecl->getNameAsString() == "operator=") {
        const DeclRefExpr *LDE = getDeclExpr(CE->getArg(0));
        const DeclRefExpr *RDE = getDeclExpr(CE->getArg(1));
        const Decl *D = getFirstDeclaration(LDE->getFoundDecl());
        if (!D) return;
        if (checkExprIsTainted(RDE, C)) {
            State = State->add<TaintDecls>(D);
            State = State->addTaint(LDE, C.getLocationContext());
        } else {
            State = State->remove<TaintDecls>(D);
        }
        if (State) {
            C.addTransition(State);
        }
    } else if (FDecl && FDecl->getNameAsString() == "operator>>"
               && FDecl->getCallResultType().getAsString().find("istream") != std::string::npos){
        for (unsigned i = 0; i < CE->getNumArgs(); ++i) {
            const Expr * E = CE->getArg(i);
            State = State->addTaint(E, C.getLocationContext());
            const DeclRefExpr *DE = getDeclExpr(E);
            if (DE) {
                const Decl * D = DE->getDecl();
                State = State->add<TaintDecls>(D);
            }
        }
        State = State->addTaint(CE, C.getLocationContext());
        const DeclRefExpr *DE = getDeclExpr(CE);
        if (DE) {
            const Decl * D = DE->getDecl();
            State = State->add<TaintDecls>(D);
        }
        if (State) {
            C.addTransition(State);
        }
    } else {
        for (unsigned i = 0; i < CE->getNumArgs(); ++i) {
            if (checkExprIsTainted(CE->getArg(i), C)) {
                State = State->addTaint(CE, C.getLocationContext());
                const DeclRefExpr *DE = getDeclExpr(CE);
                if (DE) {
                    const Decl * D = DE->getDecl();
                    State = State->add<TaintDecls>(D);
                }
                if (State) {
                    C.addTransition(State);
                    break;
                }
            }
        }

    }
}

void TaintChecker::checkPostStmt(const CXXMemberCallExpr *CE, CheckerContext &C) const {

    const Expr * object = CE->getImplicitObjectArgument();
    bool isTaintArgs = false;
    for (size_t i = 0; i < CE->getNumArgs(); ++i) {
        const Expr * Arg = CE->getArg(i);
        isTaintArgs = checkExprIsTainted(Arg, C);
        if (isTaintArgs)
            break;
    }

    if (isTaintArgs) {
        if (const DeclRefExpr *declRef = getDeclExpr(object)) {
            ProgramStateRef State = C.getState();
            Decl const *decl = declRef->getFoundDecl();
            if (decl) {
                State = State->add<TaintDecls>(decl);
                if (State) {
                    C.addTransition(State);
                }
            }
        }
    }
}

void TaintChecker::addSourcesPre(const CallExpr *CE,
                                 CheckerContext &C) const {
    ProgramStateRef State = 0;
    const FunctionDecl *FDecl = C.getCalleeDecl(CE);
    if (!FDecl || FDecl->getKind() != Decl::Function)
        return;

    StringRef Name = C.getCalleeName(FDecl);
    if (Name.empty())
        return;

    // First, try generating a propagation rule for this function.
    TaintPropagationRule Rule =
            TaintPropagationRule::getTaintPropagationRule(FDecl, Name, C, this);
    if (!Rule.isNull()) {
        State = Rule.process(CE, C, this);
        if (!State)
            return;
        C.addTransition(State);
        return;
    }
    // Otherwise, check if we have custom pre-processing implemented.
    FnCheck evalFunction = llvm::StringSwitch<FnCheck>(Name)
            .Case("fscanf", &TaintChecker::preFscanf)
            .Default(0);
    // Check and evaluate the call.
    if (evalFunction)
        State = (this->*evalFunction)(CE, C);
    if (!State) {
        return;
    }

}

bool TaintChecker::propagateFromPre(const CallExpr *CE,
                                    CheckerContext &C) const {
    ProgramStateRef State = C.getState();

    // Depending on what was tainted at pre-visit, we determined a set of
    // arguments which should be tainted after the function returns. These are
    // stored in the state as TaintArgsOnPostVisit set.
    TaintArgsOnPostVisitTy TaintArgs = State->get<TaintArgsOnPostVisit>();
    if (TaintArgs.isEmpty())
        return false;

    for (llvm::ImmutableSet<unsigned>::iterator
         I = TaintArgs.begin(), E = TaintArgs.end(); I != E; ++I) {
        unsigned ArgNum  = *I;
        if (ArgNum == ReturnValueIndex) {
            State = State->addTaint(CE, C.getLocationContext());
            continue;
        }

        // The arguments are pointer arguments. The data they are pointing at is
        // tainted after the call.
        if (CE->getNumArgs() < (ArgNum + 1))
            return false;
        const Expr* Arg = CE->getArg(ArgNum);
        if (const DeclRefExpr *declRef = getDeclExpr(Arg)) {
            Decl const *decl = declRef->getFoundDecl();
            if (decl) {
                State = State->add<TaintDecls>(decl);
            }
        }

        SymbolRef Sym = getPointedToSymbol(C, Arg);
        if (Sym)
            State = State->addTaint(Sym);
    }

    // Clear up the taint info from the state.
    State = State->remove<TaintArgsOnPostVisit>();

    if (State != C.getState()) {
        C.addTransition(State);
        return true;
    }
    return false;
}

void TaintChecker::addSourcesPost(const CallExpr *CE,
                                  CheckerContext &C) const {
    // Define the attack surface.
    // Set the evaluation function by switching on the callee name.
    const FunctionDecl *FDecl = C.getCalleeDecl(CE);
    if (!FDecl || FDecl->getKind() != Decl::Function)
        return;

    StringRef Name = C.getCalleeName(FDecl);
    if (Name.empty())
        return;
    FnCheck evalFunction = llvm::StringSwitch<FnCheck>(Name)
            .Case("scanf", &TaintChecker::postScanf)
            // TODO: Add support for vfscanf & family.
            .Case("getchar", &TaintChecker::postRetTaint)
            .Case("getchar_unlocked", &TaintChecker::postRetTaint)
            .Case("getenv", &TaintChecker::postRetTaint)
            .Case("fopen", &TaintChecker::postRetTaint)
            .Case("fdopen", &TaintChecker::postRetTaint)
            .Case("freopen", &TaintChecker::postRetTaint)
            .Case("getch", &TaintChecker::postRetTaint)
            .Case("wgetch", &TaintChecker::postRetTaint)
            .Case("socket", &TaintChecker::postSocket)
            .Default(0);

    // If the callee isn't defined, it is not of security concern.
    // Check and evaluate the call.
    ProgramStateRef State = 0;
    if (evalFunction)
        State = (this->*evalFunction)(CE, C);
    else {
        const FunctionDecl *FDecl = C.getCalleeDecl(CE);
        if (!FDecl || FDecl->getKind() != Decl::Function)
            return;

        StringRef Name = C.getCalleeName(FDecl);
        if (Name.empty())
            return;
        AddTaintingRule Rule = AddTaintingRule::getAddTaintingRule(FDecl, Name, C, this);
        if (!Rule.isNull()) {
            State = Rule.process(CE, C,this);
        }
    }
    if (!State)
        return;
    C.addTransition(State);
}


void TaintChecker::sanitize(const CallExpr *CE,
                            CheckerContext &C) const {
    const FunctionDecl *FDecl = C.getCalleeDecl(CE);
    if (!FDecl || FDecl->getKind() != Decl::Function)
        return;

    StringRef Name = C.getCalleeName(FDecl);
    if (Name.empty())
        return;
    SanitizeRule Rule = SanitizeRule::getSanitizeRule(Name, this);
    if (Rule.isNull()) {
        return;
    }

    ProgramStateRef State = 0;
    State = Rule.process(CE, C, this);
    if (State) {
        C.addTransition(State);
    }
}

bool TaintChecker::checkPre(const CallExpr *CE, CheckerContext &C) const{

    if (checkUncontrolledFormatString(CE, C))
        return true;

    const FunctionDecl *FDecl = C.getCalleeDecl(CE);
    if (!FDecl || FDecl->getKind() != Decl::Function)
        return false;

    StringRef Name = C.getCalleeName(FDecl);
    if (Name.empty())
        return false;

    if (checkSystemCall(CE, Name, C))
        return true;

    if (checkTaintedBufferSize(CE, FDecl, C))
        return true;

    if (checkUserRule(CE, Name, C))
        return true;

    return false;
}

SymbolRef TaintChecker::getPointedToSymbol(CheckerContext &C,
                                           const Expr* Arg) {
    ProgramStateRef State = C.getState();
    SVal AddrVal = State->getSVal(Arg->IgnoreParens(), C.getLocationContext());
    if (AddrVal.isUnknownOrUndef())
        return 0;

    Optional<Loc> AddrLoc = AddrVal.getAs<Loc>();
    if (!AddrLoc)
        return 0;

    const PointerType *ArgTy =
            dyn_cast<PointerType>(Arg->getType().getTypePtr());
    SVal Val = State->getSVal(*AddrLoc,
                              ArgTy ? ArgTy->getPointeeType(): QualType());
    return Val.getAsSymbol();
}

ProgramStateRef
TaintChecker::TaintPropagationRule::process(const CallExpr *CE,
                                            CheckerContext &C, const TaintChecker *Checker) const {
    ProgramStateRef State = C.getState();

    // Check for taint in arguments.
    bool IsTainted = false;
    for (ArgVector::const_iterator I = SrcArgs.begin(),
         E = SrcArgs.end(); I != E; ++I) {
        unsigned ArgNum = *I;

        if (ArgNum == InvalidArgIndex) {
            // Check if any of the arguments is tainted, but skip the
            // destination arguments.
            for (unsigned int i = 0; i < CE->getNumArgs(); ++i) {
                if (isDestinationArgument(i))
                    continue;
                if ((IsTainted = isTaintedOrPointsToTainted(CE->getArg(i), State, C, Checker)))
                    break;
                DeclRefExpr const *DE = Checker->getDeclExpr(CE->getArg(ArgNum));
                if (DE) {
                    if ((IsTainted = Checker->findInTaintDecls(DE, C))) {
                        break;
                    }
                }
            }
            break;
        }

        if (CE->getNumArgs() < (ArgNum + 1))
            return State;
        if ((IsTainted = isTaintedOrPointsToTainted(CE->getArg(ArgNum), State, C, Checker)))
            break;
    }
    if (!IsTainted) {
        return State;
    }
    // Mark the arguments which should be tainted after the function returns.
    for (ArgVector::const_iterator I = DstArgs.begin(),
         E = DstArgs.end(); I != E; ++I) {
        unsigned ArgNum = *I;

        // Should we mark all arguments as tainted?
        if (ArgNum == InvalidArgIndex) {
            // For all pointer and references that were passed in:
            //   If they are not pointing to const data, mark data as tainted.
            //   TODO: So far we are just going one level down; ideally we'd need to
            //         recurse here.
            for (unsigned int i = 0; i < CE->getNumArgs(); ++i) {

                const Expr *Arg = CE->getArg(i);
                // Process pointer argument.
                const Type *ArgTy = Arg->getType().getTypePtr();
                QualType PType = ArgTy->getPointeeType();
                if ((!PType.isNull() && !PType.isConstQualified())
                        || (ArgTy->isReferenceType() && !Arg->getType().isConstQualified())) {
                    State = State->add<TaintArgsOnPostVisit>(i);
                }
            }
            continue;
        }

        // Should mark the return value?
        if (ArgNum == ReturnValueIndex) {
            State = State->add<TaintArgsOnPostVisit>(ReturnValueIndex);
            continue;
        }

        // Mark the given argument.
        assert(ArgNum < CE->getNumArgs());
        DeclRefExpr const * DE = Checker->getDeclExpr(CE->getArg(ArgNum));
        if (DE) {
            Decl const *decl = DE->getFoundDecl();
            if(decl) {
                State = State->add<TaintDecls>(decl);
            }
        }
        State = State->add<TaintArgsOnPostVisit>(ArgNum);
    }

    return State;
}


// If argument 0 (file descriptor) is tainted, all arguments except for arg 0
// and arg 1 should get taint.
ProgramStateRef TaintChecker::preFscanf(const CallExpr *CE,
                                        CheckerContext &C) const {
    assert(CE->getNumArgs() >= 2);
    ProgramStateRef State = C.getState();

    // Check is the file descriptor is tainted.
    if (State->isTainted(CE->getArg(0), C.getLocationContext()) ||
            isStdin(CE->getArg(0), C)) {
        // All arguments except for the first two should get taint.
        for (unsigned int i = 2; i < CE->getNumArgs(); ++i)
            State = State->add<TaintArgsOnPostVisit>(i);
        return State;
    }

    return 0;
}


// If argument 0(protocol domain) is network, the return value should get taint.
ProgramStateRef TaintChecker::postSocket(const CallExpr *CE,
                                         CheckerContext &C) const {
    ProgramStateRef State = C.getState();
    if (CE->getNumArgs() < 3)
        return State;

    SourceLocation DomLoc = CE->getArg(0)->getExprLoc();
    StringRef DomName = C.getMacroNameOrSpelling(DomLoc);
    // White list the internal communication protocols.
    if (DomName.equals("AF_SYSTEM") || DomName.equals("AF_LOCAL") ||
            DomName.equals("AF_UNIX") || DomName.equals("AF_RESERVED_36"))
        return State;
    State = State->addTaint(CE, C.getLocationContext());
    return State;
}

ProgramStateRef TaintChecker::postScanf(const CallExpr *CE,
                                        CheckerContext &C) const {
    ProgramStateRef State = C.getState();
    if (CE->getNumArgs() < 2)
        return State;

    // All arguments except for the very first one should get taint.
    for (unsigned int i = 1; i < CE->getNumArgs(); ++i) {
        // The arguments are pointer arguments. The data they are pointing at is
        // tainted after the call.
        const Expr* Arg = CE->getArg(i);
        SymbolRef Sym = getPointedToSymbol(C, Arg);
        if (Sym)
            State = State->addTaint(Sym);
    }
    return State;
}

ProgramStateRef TaintChecker::postRetTaint(const CallExpr *CE,
                                           CheckerContext &C) const {
    const Expr *E;
    if ((E = dyn_cast_or_null<Expr>(CE)))
        E = E->IgnoreParens();
    SymbolRef Sym = C.getSVal(E).getAsSymbol();
    if (Sym) {
        return C.getState()->addTaint(Sym);
    }
    return C.getState()->addTaint(CE, C.getLocationContext());
}

bool TaintChecker::isStdin(const Expr *E, CheckerContext &C) {
    ProgramStateRef State = C.getState();
    SVal Val = State->getSVal(E, C.getLocationContext());

    // stdin is a pointer, so it would be a region.
    const MemRegion *MemReg = Val.getAsRegion();

    // The region should be symbolic, we do not know it's value.
    const SymbolicRegion *SymReg = dyn_cast_or_null<SymbolicRegion>(MemReg);
    if (!SymReg)
        return false;

    // Get it's symbol and find the declaration region it's pointing to.
    const SymbolRegionValue *Sm =dyn_cast<SymbolRegionValue>(SymReg->getSymbol());
    if (!Sm)
        return false;
    const DeclRegion *DeclReg = dyn_cast_or_null<DeclRegion>(Sm->getRegion());
    if (!DeclReg)
        return false;

    // This region corresponds to a declaration, find out if it's a global/extern
    // variable named stdin with the proper type.
    if (const VarDecl *D = dyn_cast_or_null<VarDecl>(DeclReg->getDecl())) {
        D = D->getCanonicalDecl();
        if ((D->getName().find("stdin") != StringRef::npos) && D->isExternC())
            if (const PointerType * PtrTy =
                    dyn_cast<PointerType>(D->getType().getTypePtr()))
                if (PtrTy->getPointeeType() == C.getASTContext().getFILEType())
                    return true;
    }
    return false;
}

static bool getPrintfFormatArgumentNum(const CallExpr *CE,
                                       const CheckerContext &C,
                                       unsigned int &ArgNum) {
    // Find if the function contains a format string argument.
    // Handles: fprintf, printf, sprintf, snprintf, vfprintf, vprintf, vsprintf,
    // vsnprintf, syslog, custom annotated functions.
    const FunctionDecl *FDecl = C.getCalleeDecl(CE);
    if (!FDecl)
        return false;
    for (specific_attr_iterator<FormatAttr>
         i = FDecl->specific_attr_begin<FormatAttr>(),
         e = FDecl->specific_attr_end<FormatAttr>(); i != e ; ++i) {

        const FormatAttr *Format = *i;
        ArgNum = Format->getFormatIdx() - 1;
        if ((Format->getType() == "printf") && CE->getNumArgs() > ArgNum)
            return true;
    }

    // Or if a function is named setproctitle (this is a heuristic).
    if (C.getCalleeName(CE).find("setproctitle") != StringRef::npos) {
        ArgNum = 0;
        return true;
    }

    return false;
}

bool TaintChecker::generateReportIfTainted(const Expr *E,
                                           const char Msg[],
                                           CheckerContext &C) const {
    assert(E);

    // Check for taint.
    ProgramStateRef State = C.getState();
    if (!checkExprIsTainted(E, C))
        return false;
    StringRef FileName = C.getSourceManager().getFilename(E->getExprLoc());
    if (!FileName.empty()) {
        std::map<StringRef, std::vector<const Expr *> *>::const_iterator it = TaintErrorCalls->find(FileName);
        if (it != TaintErrorCalls->end()) {
            it->second->push_back(E);
        } else {
            std::vector<const Expr *> *Vec = new std::vector<const Expr *>();
            Vec->push_back(E);
            TaintErrorCalls->insert(std::make_pair(FileName, Vec));
        }
    }
    // Generate diagnostic.
    if (ExplodedNode *N = C.addTransition()) {
        initBugType();
        BugReport *report = new BugReport(*BT, Msg, N);
        report->addRange(E->getSourceRange());
        C.emitReport(report);
        return true;
    }
    return false;
}

bool TaintChecker::checkUncontrolledFormatString(const CallExpr *CE,
                                                 CheckerContext &C) const{
    // Check if the function contains a format string argument.
    unsigned int ArgNum = 0;
    if (!getPrintfFormatArgumentNum(CE, C, ArgNum))
        return false;

    // If either the format string content or the pointer itself are tainted, warn.
    if (generateReportIfTainted(CE->getArg(ArgNum),
                                MsgUncontrolledFormatString, C))
        return true;
    return false;
}

bool TaintChecker::checkSystemCall(const CallExpr *CE,
                                   StringRef Name,
                                   CheckerContext &C) const {
    // TODO: It might make sense to run this check on demand. In some cases,
    // we should check if the environment has been cleansed here. We also might
    // need to know if the user was reset before these calls(seteuid).
    unsigned ArgNum = llvm::StringSwitch<unsigned>(Name)
            .Case("system", 0)
            .Case("popen", 0)
            .Case("execl", 0)
            .Case("execle", 0)
            .Case("execlp", 0)
            .Case("execv", 0)
            .Case("execvp", 0)
            .Case("execvP", 0)
            .Case("execve", 0)
            .Case("dlopen", 0)
            .Case("execStr", 0)
            .Default(UINT_MAX);

    if (ArgNum == UINT_MAX || CE->getNumArgs() < (ArgNum + 1))
        return false;

    if (generateReportIfTainted(CE->getArg(ArgNum),
                                MsgSanitizeSystemArgs, C))
        return true;

    return false;
}

// TODO: Should this check be a part of the CString checker?
// If yes, should taint be a global setting?
bool TaintChecker::checkTaintedBufferSize(const CallExpr *CE,
                                          const FunctionDecl *FDecl,
                                          CheckerContext &C) const {
    // If the function has a buffer size argument, set ArgNum.
    unsigned ArgNum = InvalidArgIndex;
    unsigned BId = 0;
    if ( (BId = FDecl->getMemoryFunctionKind()) )
        switch(BId) {
        case Builtin::BImemcpy:
        case Builtin::BImemmove:
        case Builtin::BIstrncpy:
            ArgNum = 2;
            break;
        case Builtin::BIstrndup:
            ArgNum = 1;
            break;
        default:
            break;
        };

    if (ArgNum == InvalidArgIndex) {
        if (C.isCLibraryFunction(FDecl, "malloc") ||
                C.isCLibraryFunction(FDecl, "calloc") ||
                C.isCLibraryFunction(FDecl, "alloca"))
            ArgNum = 0;
        else if (C.isCLibraryFunction(FDecl, "memccpy"))
            ArgNum = 3;
        else if (C.isCLibraryFunction(FDecl, "realloc"))
            ArgNum = 1;
        else if (C.isCLibraryFunction(FDecl, "bcopy"))
            ArgNum = 2;
    }

    if (ArgNum != InvalidArgIndex && CE->getNumArgs() > ArgNum &&
            generateReportIfTainted(CE->getArg(ArgNum), MsgTaintedBufferSize, C))
        return true;

    return false;
}

bool TaintChecker::checkUserRule(const CallExpr *CE,
                                 StringRef Name,
                                 CheckerContext &C) const {
    TaintingErrorRule Rule = TaintingErrorRule::getTaintingErrorRule(Name, this);
    if (!Rule.isNull()) {
        ProgramStateRef State = 0;
        State = Rule.process(CE, C, this);
        if (!State)
            return false;
        C.addTransition(State);
        return true;
    }

    return false;
}

void TaintChecker::generateSanitizeFunctionFile() const{
    std::ofstream os;
    os.open("sanitize.h");
    if (!os.is_open()) return;
    os << "#ifndef SANITIZE_H" << std::endl;
    os << "#define SANITIZE_H" << std::endl;
    os << "namespace tainting_namespace {" << std::endl;
    os << "#include <string>" << std::endl;
    os << "namespace tainting_namespace {" << std::endl;
    os << "std::string sanitize(std::string const& str) const {" << std::endl;
    os << "\tstd::string result = str;" << std::endl;
    os << "\tunsigned pos;" << std::endl;
    const std::map<std::string, std::string> ESRules = ConfigParser.getReplaceSequences();
    for (std::map<std::string, std::string>::const_iterator it = ESRules.begin(); it != ESRules.end(); ++it) {
        os << "\tpos = 0;"<< std::endl;
        os << "\twhile((curPos = result.find(\"" + it->first + "\", pos)) != std::string::npos) {"<< std::endl;
        os << "\t\tresutl.replace(pos, " << it->first.size() << ", \"" << it->second << "\");" << std::endl;
        os << "\t\t++pos;\n \t}" << std::endl;
    }
    os << "}" << std::endl;
    os << "}" << std::endl;
    os << "#endif" << std::endl;
}

unsigned TaintChecker::getExpressionLength(const Expr * E) const {
    std::string str;
    clang::LangOptions LangOpts;
    LangOpts.CPlusPlus = true;
    clang::PrintingPolicy Policy(LangOpts);
    llvm::raw_string_ostream s(str);
    E->printPretty(s, 0, Policy);
    return s.str().length();
}

void TaintChecker::checkEndAnalysis(ExplodedGraph &G, BugReporter &B,
                                    ExprEngine &Eng) const {
    if (TaintErrorCalls->empty()) {
        return;
    }
    std::cout << "Files was created due analysis:" << std::endl;
    SourceManager& SM = B.getSourceManager();
    for (std::map<StringRef, std::vector<const Expr *> *>::const_iterator it = TaintErrorCalls->begin();
         it != TaintErrorCalls->end(); ++ it) {
        std::cout << "\t - " << std::string(it->first) + ".out" << std::endl;
        //process file
        std::ifstream is;
        is.open(std::string(it->first).c_str());
        if (!is.is_open()) continue;
        std::ofstream os;
        os.open( (std::string(it->first) + ".out").c_str());
        if (!os.is_open()) continue;
        unsigned line_num = 0;
        std::string line;
        os << "#include \"sanitize.h\"" << std::endl;
        std::vector<const Expr *>::const_iterator TEIt = it->second->begin();
        FileID FID = SM.getFileID((*TEIt)->getExprLoc());
        unsigned Begin = SM.getFileOffset((*TEIt)->getExprLoc());
        unsigned ExprLine = SM.getLineNumber(FID, Begin);
        unsigned ExprCol = SM.getColumnNumber(FID, Begin);
        while (std::getline(is, line)) {
            unsigned start = 0, end = 0;
            if ((ExprLine - 1) == line_num) {
                std::string str;
                while (TEIt != it->second->end() && (ExprLine - 1) == line_num) {
                    unsigned exprSize = getExpressionLength(*TEIt);
                    end = ExprCol - 1;
                    str  += line.substr(start, end) + "tainting_namespace::sanitize(" + line.substr(end, exprSize) + ")";
                    start = end + exprSize;
                    ++TEIt;
                    if (TEIt != it->second->end()) {
                        FID = SM.getFileID((*TEIt)->getExprLoc());
                        Begin = SM.getFileOffset((*TEIt)->getExprLoc());
                        ExprLine = SM.getLineNumber(FID, Begin);
                        ExprCol = SM.getColumnNumber(FID, Begin);
                    }
                }
                str += line.substr(start, line.size() - start);
                os << str << std::endl;
            } else {
                os << line << std::endl;
            }
            ++line_num;
        }
    }
    //sanitize file
    std::cout << "\t - sanitize.h" << std::endl;
    generateSanitizeFunctionFile();
}

void ento::registerTaintChecker(CheckerManager &mgr) {
    mgr.registerChecker<TaintChecker>();
}
