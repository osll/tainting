#include <iostream>
#include <cstdlib> 
#include <fstream>

int main(int argc, char const *argv[])
{
	std::ifstream is;
	std::string str1;
	std::string str2;
	
	std::cin >> str1;	
	is >> str2;

	system(str1.c_str());		// taint error
	system(str2.c_str());		// taint error
	return 0;
}