#include <string>
#include <cstdlib> 


std::string getTaintString () {
	return "abc";
}

void copyString(std::string const& src, std::string& dst) {
	dst = src;
}

std::string validate(std::string& taintString) {
	return std::string(taintString);
}

void makeSomeDangerous(std::string const& param) {
	system(param.c_str());
}

int main(int argc, char const *argv[])
{
	std::string taintStr = getTaintString();
	std::string str1;
	std::string str2;
	std::string cleanString = "qwerty";
	
	copyString(taintStr, str1);
	copyString(cleanString, str2);

	makeSomeDangerous(str1);				// taint error
	makeSomeDangerous(validate(str1)); 		// ok
	makeSomeDangerous(str2);				// ok
	makeSomeDangerous(validate(str2));		// ok
	return 0;
}