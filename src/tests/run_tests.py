import os

def scan_source(filename):
	errors = 0
	f = open(filename, "r")
	for line in f:
		if line.find("taint error") != -1:
			errors += 1
	return errors

def parse_log(log):
	f = open(log, "r")
	status = True
	found_errors = 0
	has_warning = False
	for line in f:		
		if has_warning:
			if line.find("taint error") != -1:
				found_errors += 1
			else:
				status = False				
			has_warning = False
		if line.find("warning: Untrusted data") != -1:
			has_warning = True

	return status, found_errors

def run_test(name, filename):
	os.system("%s %s 2> %s > %s" % ("scan-build -enable-checker alpha.security.taint g++", filename, "temp1", "temp2"))
	expected_errors = scan_source(filename)
	status, found_errors = parse_log("temp1")
	if status and expected_errors == found_errors:
		print "%s passed. " % name
	else:
		print "%s failed. found errors %d of %d" % (name, found_errors, expected_errors)
	
	os.remove("temp1")
	os.remove("temp2")

def run_tests():
	tests = {
		"string test": "string_test.cpp",
		"user rules test": "user_rules_test.cpp",
		"istream test": "stream_test.cpp"
		}
	for test_name in tests:
		run_test(test_name, tests[test_name])

run_tests()