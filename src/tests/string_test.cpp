#include <cstdlib> 
#include <string>
#include <cstdio>
#include <string.h>

using std::string;

int main() {
  char *c = getenv("HOME");       // c tainted

  //taint arguments
  char c2[24];  
  memcpy(c2, c, strlen(c));       // c2 tainted

  string taint_string = c2;       // taint_string tainted
  string str_prop;
  str_prop = taint_string + "random const string";        // str_prop tainted
  
  system(str_prop.c_str());       // taint error
  system(c);                      // taint error
  system(c2);                     // taint error

  return 0;
}