//===-- taintsan.h ----------------------------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file is a part of TaintSanitizer.
//
// Private TaintSan header.
//===----------------------------------------------------------------------===//

#ifndef TAINTSAN_H
#define TAINTSAN_H

#include "sanitizer_common/sanitizer_flags.h"
#include "sanitizer_common/sanitizer_internal_defs.h"
#include "sanitizer_common/sanitizer_stacktrace.h"
#include "taintsan_interface_internal.h"
#include "taintsan_flags.h"

#ifndef TAINTSAN_REPLACE_OPERATORS_NEW_AND_DELETE
# define TAINTSAN_REPLACE_OPERATORS_NEW_AND_DELETE 1
#endif

#define MEM_TO_SHADOW(mem) (((uptr)mem)       & ~0x400000000000ULL)
#define MEM_IS_APP(mem)    ((uptr)mem >=         0x600000000000ULL)
#define MEM_IS_SHADOW(mem) ((uptr)mem >=         0x200000000000ULL && \
                            (uptr)mem <=         0x400000000000ULL)

struct link_map;  // Opaque type returned by dlopen().

const int kTaintsanParamTlsSizeInWords = 100;
const int kTaintsanRetvalTlsSizeInWords = 100;

namespace __taintsan {
extern int taintsan_inited;
extern bool taintsan_init_is_running;
extern int taintsan_report_count;

bool InitShadow(bool prot1, bool prot2, bool map_shadow);
void InitializeInterceptors();

void *TaintsanReallocate(StackTrace *stack, void *oldp, uptr size,
                     uptr alignment, bool zeroise);
void TaintsanDeallocate(void *ptr); 
void InstallAtExitHandler();        
void ReplaceOperatorsNewAndDelete();

void EnterSymbolizer();
void ExitSymbolizer();
bool IsInSymbolizer();

struct SymbolizerScope {
  SymbolizerScope() { EnterSymbolizer(); }
  ~SymbolizerScope() { ExitSymbolizer(); }
};

void TaintsanDie();
void PrintWarning(uptr pc, uptr bp, const char * info);

void GetStackTrace(StackTrace *stack, uptr max_s, uptr pc, uptr bp,
                   bool fast);

void ReportTainted(StackTrace *stack, const char * info);
void ReportAtExitStatistics();

void UnpoisonMappedDSO(struct link_map *map); 

}  // namespace __taintsan
#endif // TAINTSAN_H
