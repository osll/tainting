//===-- taintsan_report.cc ------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file is a part of TaintSanitizer.
//
// Error reporting.
//===----------------------------------------------------------------------===//

#include "taintsan.h"
#include "sanitizer_common/sanitizer_common.h"
#include "sanitizer_common/sanitizer_flags.h"
#include "sanitizer_common/sanitizer_mutex.h"
#include "sanitizer_common/sanitizer_report_decorator.h"
#include "sanitizer_common/sanitizer_stackdepot.h"
#include "sanitizer_common/sanitizer_symbolizer.h"

using namespace __sanitizer;

namespace __taintsan {

static bool PrintsToTtyCached() {
  static int cached = 0;
  static bool prints_to_tty;
  if (!cached) {  // Ok wrt threads since we are printing only from one thread.
    prints_to_tty = PrintsToTty();
    cached = 1;
  }
  return prints_to_tty;
}

class Decorator: private __sanitizer::AnsiColorDecorator {
 public:
  Decorator() : __sanitizer::AnsiColorDecorator(PrintsToTtyCached()) { }
  const char *Warning()    { return Red(); }
  const char *Name()   { return Green(); }
  const char *End()    { return Default(); }
};

static void PrintStack(const uptr *trace, uptr size) {
  SymbolizerScope sym_scope;
  StackTrace::PrintStack(trace, size, true,
                         common_flags()->strip_path_prefix, 0);
}

static void ReportSummary(const char *error_type, StackTrace *stack) {
  if (!stack->size || !IsSymbolizerAvailable()) return;
  AddressInfo ai;
  uptr pc = StackTrace::GetPreviousInstructionPc(stack->trace[0]);
  {
    SymbolizerScope sym_scope;
    SymbolizeCode(pc, &ai, 1);
  }
  ReportErrorSummary(error_type,
                     StripPathPrefix(ai.file,
                                     common_flags()->strip_path_prefix),
                     ai.line, ai.function);
}

void ReportTainted(StackTrace *stack, const char * info) {
  if (!__taintsan::flags()->report_taint) return;

  SpinMutexLock l(&CommonSanitizerReportMutex);

  Decorator d;
  Printf("%s", d.Warning());
  Report(" WARNING: Use of tainted data in sink\n");
  Printf("%s", d.End());
  Printf("sink: %s\n", info);
  PrintStack(stack->trace, stack->size);
  ReportSummary("use-of-tainted-data", stack);
}

void ReportAtExitStatistics() {
  SpinMutexLock l(&CommonSanitizerReportMutex);

  Decorator d;
  Printf("%s", d.Warning());
  Printf("TaintSanitizer: %d warnings reported.\n", taintsan_report_count);
  Printf("%s", d.End());
}


}  // namespace __taintsan
