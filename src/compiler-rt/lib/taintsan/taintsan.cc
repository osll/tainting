//===-- taintsan.cc -------------------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file is a part of TaintSanitizer.
//
// TaintSanitizer runtime.
//===----------------------------------------------------------------------===//

#include "taintsan.h"
#include "sanitizer_common/sanitizer_atomic.h"
#include "sanitizer_common/sanitizer_common.h"
#include "sanitizer_common/sanitizer_flags.h"
#include "sanitizer_common/sanitizer_libc.h"
#include "sanitizer_common/sanitizer_procmaps.h"
#include "sanitizer_common/sanitizer_stacktrace.h"
#include "sanitizer_common/sanitizer_symbolizer.h"

#include "interception/interception.h"

using namespace __sanitizer;

// Globals.
static THREADLOCAL int taintsan_expect_taint = 0;
static THREADLOCAL int taintsan_expected_taint_found = 0;

SANITIZER_INTERFACE_ATTRIBUTE
THREADLOCAL u64 __taintsan_param_tls[kTaintsanParamTlsSizeInWords];

SANITIZER_INTERFACE_ATTRIBUTE
THREADLOCAL u64 __taintsan_retval_tls[kTaintsanRetvalTlsSizeInWords];

SANITIZER_INTERFACE_ATTRIBUTE
THREADLOCAL u64 __taintsan_va_arg_tls[kTaintsanParamTlsSizeInWords];

SANITIZER_INTERFACE_ATTRIBUTE
THREADLOCAL u64 __taintsan_va_arg_overflow_size_tls;

static THREADLOCAL struct {
  uptr stack_top, stack_bottom;
} __taintsan_stack_bounds;

static THREADLOCAL bool is_in_symbolizer;

namespace __taintsan {

void EnterSymbolizer() { is_in_symbolizer = true; }
void ExitSymbolizer()  { is_in_symbolizer = false; }
bool IsInSymbolizer() { return is_in_symbolizer; }

static Flags taintsan_flags;

Flags *flags() {
  return &taintsan_flags;
}

int taintsan_inited = 0;
bool taintsan_init_is_running;

int taintsan_report_count = 0;

static void ParseFlagsFromString(Flags *f, const char *str) {
  ParseCommonFlagsFromString(str);
  ParseFlag(str, &f->exit_code, "exit_code");
  if (f->exit_code < 0 || f->exit_code > 127) {
    Printf("Exit code not in [0, 128) range: %d\n", f->exit_code);
    f->exit_code = 1;
    Die();
  }
  ParseFlag(str, &f->report_taint, "report_taint");
  ParseFlag(str, &f->verbosity, "verbosity");
}

static void InitializeFlags(Flags *f, const char *options) {
  CommonFlags *cf = common_flags();
  cf->external_symbolizer_path = GetEnv("TAINTSAN_SYMBOLIZER_PATH");
  cf->strip_path_prefix = "";
  cf->fast_unwind_on_fatal = false;
  cf->fast_unwind_on_malloc = true;
  cf->malloc_context_size = 20;

  internal_memset(f, 0, sizeof(*f));
  f->exit_code = 77;
  f->report_taint = true;
  f->verbosity = 0;

  // Override from user-specified string.
  if (__taintsan_default_options)
    ParseFlagsFromString(f, __taintsan_default_options());
  ParseFlagsFromString(f, options);
}

static void GetCurrentStackBounds(uptr *stack_top, uptr *stack_bottom) {
  if (__taintsan_stack_bounds.stack_top == 0) {
    // Break recursion (GetStackTrace -> GetThreadStackTopAndBottom ->
    // realloc -> GetStackTrace).
    __taintsan_stack_bounds.stack_top = __taintsan_stack_bounds.stack_bottom = 1;
    GetThreadStackTopAndBottom(/* at_initialization */false,
                               &__taintsan_stack_bounds.stack_top,
                               &__taintsan_stack_bounds.stack_bottom);
  }
  *stack_top = __taintsan_stack_bounds.stack_top;
  *stack_bottom = __taintsan_stack_bounds.stack_bottom;
}

void GetStackTrace(StackTrace *stack, uptr max_s, uptr pc, uptr bp,
                   bool fast) {
  if (!fast) {
    // Block reports from our interceptors during _Unwind_Backtrace.
    SymbolizerScope sym_scope;
    return stack->SlowUnwindStack(pc, max_s);
  }

  uptr stack_top, stack_bottom;
  GetCurrentStackBounds(&stack_top, &stack_bottom);
  stack->size = 0;
  stack->trace[0] = pc;
  stack->max_size = max_s;
  stack->FastUnwindStack(pc, bp, stack_top, stack_bottom);
}

void PrintWarning(uptr pc, uptr bp, const char * info) {
  if (taintsan_expect_taint) {
    taintsan_expected_taint_found = 1;
    return;
  }

  ++taintsan_report_count;

  StackTrace stack;
  GetStackTrace(&stack, kStackTraceMax, pc, bp,
                common_flags()->fast_unwind_on_fatal);

  ReportTainted(&stack, info);
}

}  // namespace __taintsan

// Interface.

using namespace __taintsan;

void __taintsan_warning(const char * info) {
  GET_CALLER_PC_BP_SP;
  (void)sp;
  PrintWarning(pc, bp, info);
}

void __taintsan_warning_noreturn(const char * info) {
  GET_CALLER_PC_BP_SP;
  (void)sp;
  PrintWarning(pc, bp, info);
  Printf("Exiting\n");
  Die();
}

void __taintsan_init() {
  if (taintsan_inited) return;
  taintsan_init_is_running = 1;
  SanitizerToolName = "TaintSanitizer";

  InstallAtExitHandler();
  SetDieCallback(TaintsanDie);
  InitTlsSize();
  InitializeInterceptors();

  if (TAINTSAN_REPLACE_OPERATORS_NEW_AND_DELETE)
    ReplaceOperatorsNewAndDelete();
  const char *taintsan_options = GetEnv("TAINTSAN_OPTIONS");
  InitializeFlags(&taintsan_flags, taintsan_options);
  if (StackSizeIsUnlimited()) {
    if (flags()->verbosity)
      Printf("Unlimited stack, doing reexec\n");
    // A reasonably large stack size. It is bigger than the usual 8Mb, because,
    // well, the program could have been run with unlimited stack for a reason.
    SetStackSizeLimitInBytes(32 * 1024 * 1024);
    ReExec();
  }

  if (flags()->verbosity)
    Printf("TAINTSAN_OPTIONS: %s\n", taintsan_options ? taintsan_options : "<empty>");

  __taintsan_clear_on_return();
  if (!InitShadow(/* prot1 */false, /* prot2 */true, /* map_shadow */true)) {
    // FIXME: prot1 = false is only required when running under DR.
    Printf("FATAL: TaintSanitizer can not mmap the shadow memory.\n");
    Printf("FATAL: Make sure to compile with -fPIE and to link with -pie.\n");
    Printf("FATAL: Disabling ASLR is known to cause this error.\n");
    Printf("FATAL: If running under GDB, try "
           "'set disable-randomization off'.\n");
    DumpProcessMap();
    Die();
  }

  const char *external_symbolizer = common_flags()->external_symbolizer_path;
  if (external_symbolizer && external_symbolizer[0]) {
    CHECK(InitializeExternalSymbolizer(external_symbolizer));
  }

  GetThreadStackTopAndBottom(/* at_initialization */true,
                             &__taintsan_stack_bounds.stack_top,
                             &__taintsan_stack_bounds.stack_bottom);
  if (flags()->verbosity)
    Printf("TaintSanitizer init done\n");
  taintsan_init_is_running = 0;
  taintsan_inited = 1;
}

void __taintsan_set_exit_code(int exit_code) {
  flags()->exit_code = exit_code;
}

sptr __taintsan_test_shadow(const void *x, uptr size) {
  unsigned char *s = (unsigned char*)MEM_TO_SHADOW((uptr)x);
  for (uptr i = 0; i < size; ++i)
    if (s[i])
      return i;
  return -1;
}

NOINLINE
void __taintsan_clear_on_return() {
  __taintsan_param_tls[0] = 0;
}

void __taintsan_partial_taint(const void* data, void* shadow, uptr size) {
  internal_memcpy((void*)MEM_TO_SHADOW((uptr)data), shadow, size);
}

void __taintsan_load_untainted(void *src, uptr size, void *dst) {
  internal_memcpy(dst, src, size);
  __taintsan_untaint(dst, size);
}

#if !SANITIZER_SUPPORTS_WEAK_HOOKS
extern "C" {
SANITIZER_WEAK_ATTRIBUTE SANITIZER_INTERFACE_ATTRIBUTE
const char* __taintsan_default_options() { return ""; }
}  // extern "C"
#endif

