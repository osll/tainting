//===-- taintsan_allocator.cc ----------------------- ---------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file is a part of TaintSanitizer.
//
// TaintSanitizer allocator.
//===----------------------------------------------------------------------===//

#include "sanitizer_common/sanitizer_allocator.h"
#include "sanitizer_common/sanitizer_stackdepot.h"
#include "taintsan.h"

namespace __taintsan {

struct Metadata {
  uptr requested_size;
};

static const uptr kAllocatorSpace = 0x600000000000ULL;
static const uptr kAllocatorSize   = 0x80000000000;  // 8T.
static const uptr kMetadataSize  = sizeof(Metadata);

typedef SizeClassAllocator64<kAllocatorSpace, kAllocatorSize, kMetadataSize,
                             DefaultSizeClassMap> PrimaryAllocator;
typedef SizeClassAllocatorLocalCache<PrimaryAllocator> AllocatorCache;
typedef LargeMmapAllocator<> SecondaryAllocator;
typedef CombinedAllocator<PrimaryAllocator, AllocatorCache,
                          SecondaryAllocator> Allocator;

static THREADLOCAL AllocatorCache cache;
static Allocator allocator;

static int inited = 0;

static inline void Init() {
  if (inited) return;
  __taintsan_init();
  inited = true;  // this must happen before any threads are created.
  allocator.Init();
}

static void *TaintsanAllocate(StackTrace *stack, uptr size, uptr alignment) {
  Init();
  void *res = allocator.Allocate(&cache, size, alignment, false);
  Metadata *meta = reinterpret_cast<Metadata*>(allocator.GetMetaData(res));
  meta->requested_size = size;
  __taintsan_untaint(res, size);
  return res;
}

void TaintsanDeallocate(void *p) {
  CHECK(p);
  Init();
  Metadata *meta = reinterpret_cast<Metadata*>(allocator.GetMetaData(p));
  uptr size = meta->requested_size;
  __taintsan_untaint(p, size);   
  allocator.Deallocate(&cache, p);
}

void *TaintsanReallocate(StackTrace *stack, void *old_p, uptr new_size,
                     uptr alignment, bool zeroise) {
  if (!old_p)
    return TaintsanAllocate(stack, new_size, alignment);
  if (!new_size) {
    TaintsanDeallocate(old_p);
    return 0;
  }
  Metadata *meta = reinterpret_cast<Metadata*>(allocator.GetMetaData(old_p));
  uptr old_size = meta->requested_size;
  uptr actually_allocated_size = allocator.GetActuallyAllocatedSize(old_p);
  if (new_size <= actually_allocated_size) {
    // We are not reallocating here.
    meta->requested_size = new_size;
    if (new_size > old_size)
      __taintsan_untaint((char*)old_p + old_size, new_size - old_size);
    return old_p;
  }
  uptr memcpy_size = Min(new_size, old_size);
  void *new_p = TaintsanAllocate(stack, new_size, alignment);
  if (new_p)
    __taintsan_memcpy(new_p, old_p, memcpy_size);
  TaintsanDeallocate(old_p);
  return new_p;
}

} //__taintsan