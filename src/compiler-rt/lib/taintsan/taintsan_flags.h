//===-- taintsan_flags.h ----------------------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file is a part of TaintSanitizer.
//
// TaintSanitizer allocator.
//===----------------------------------------------------------------------===//

#ifndef TAINTSAN_FLAGS_H
#define TAINTSAN_FLAGS_H

namespace __taintsan {
	struct Flags {
		int exit_code;
		int verbosity;
		bool report_taint;
	};
	Flags *flags();
}

#endif //TAINTSAN_FLAGS_H