//===-- taintsan_interceptors.cc ------------------------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file is a part of TaintSanitizer.
//
// Interceptors for standard library functions.
//
//===----------------------------------------------------------------------===//

#include "interception/interception.h"
#include "taintsan.h"
#include "sanitizer_common/sanitizer_platform_limits_posix.h"
#include "sanitizer_common/sanitizer_allocator.h"
#include "sanitizer_common/sanitizer_common.h"
#include "sanitizer_common/sanitizer_stackdepot.h"
#include "sanitizer_common/sanitizer_libc.h"
#include "sanitizer_common/sanitizer_linux.h"

#include <stdarg.h>

extern "C" const int __taintsan_keep_going;

using namespace __taintsan;

#define ENSURE_TAINTSAN_INITED() do { \
  CHECK(!taintsan_init_is_running); \
  if (!taintsan_inited) { \
    __taintsan_init(); \
  } \
} while (0)

#define CHECK_UNTAINTED(x, n, name) \
  do { \
    sptr offset = __taintsan_test_shadow(x, n);                 \
    if (__taintsan::IsInSymbolizer()) break;                    \
    if (offset >= 0 && __taintsan::flags()->report_taint) {      \
      GET_CALLER_PC_BP_SP;                                  \
      (void)sp;                                             \
      Printf("TAINT in %s at offset %d inside [%p, +%d) \n",  \
             __FUNCTION__, offset, x, n);                   \
      __taintsan::PrintWarning(                   \
        pc, bp, name);  \
      if (!__taintsan_keep_going) {                         \
        Printf("Exiting\n");                                \
        Die();                                              \
      }                                                     \
    }                                                       \
  } while (0)

static void *fast_memset(void *ptr, int c, SIZE_T n);
static void *fast_memcpy(void *dst, const void *src, SIZE_T n);

INTERCEPTOR(SIZE_T, fread, void *ptr, SIZE_T size, SIZE_T nmemb, void *file) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T res = REAL(fread)(ptr, size, nmemb, file);
  if (res > 0)
    __taintsan_add_taint(ptr, res *size);
  return res;
}

INTERCEPTOR(SIZE_T, fread_unlocked, void *ptr, SIZE_T size, SIZE_T nmemb,
            void *file) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T res = REAL(fread_unlocked)(ptr, size, nmemb, file);
  if (res > 0)
    __taintsan_add_taint(ptr, res *size);
  return res;
}

INTERCEPTOR(void *, memcpy, void *dest, const void *src, SIZE_T n) {
  return __taintsan_memcpy(dest, src, n);
}

INTERCEPTOR(void *, memmove, void *dest, const void *src, SIZE_T n) {
  return __taintsan_memmove(dest, src, n);
}

INTERCEPTOR(void *, memset, void *s, int c, SIZE_T n) {
  return __taintsan_memset(s, c, n);
}

INTERCEPTOR(void, free, void *ptr) {
  ENSURE_TAINTSAN_INITED();
  if (ptr == 0) return;
  TaintsanDeallocate(ptr);
}

INTERCEPTOR(SIZE_T, strlen, const char *s) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T res = REAL(strlen)(s);
  //__taintsan_propagate(&res, s, sizeof(res));
  __taintsan_add_taint(&res, sizeof(res));
  //CHECK_UNTAINTED(s, res + 1);
  return res;
}

INTERCEPTOR(SIZE_T, strnlen, const char *s, SIZE_T n) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T res = REAL(strnlen)(s, n);
  SIZE_T scan_size = (res == n) ? res : res + 1;
  CHECK_UNTAINTED(s, scan_size, "strnlen");
  return res;
}

INTERCEPTOR(char *, strcpy, char *dest, const char *src) { 
  ENSURE_TAINTSAN_INITED();
  SIZE_T n = REAL(strlen)(src);
  char *res = REAL(strcpy)(dest, src); 
  __taintsan_propagate(dest, src, n + 1);
  return res;
}

INTERCEPTOR(char *, strncpy, char *dest, const char *src, SIZE_T n) { 
  ENSURE_TAINTSAN_INITED();
  SIZE_T copy_size = REAL(strnlen)(src, n);
  if (copy_size < n)
    copy_size++;  // trailing \0
  char *res = REAL(strncpy)(dest, src, n);  
  __taintsan_propagate(dest, src, copy_size);
  return res;
}

INTERCEPTOR(char *, strdup, char *src) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T n = REAL(strlen)(src);
  char *res = REAL(strdup)(src);
  __taintsan_propagate(res, src, n + 1);
  return res;
}

INTERCEPTOR(char *, __strdup, char *src) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T n = REAL(strlen)(src);
  char *res = REAL(__strdup)(src);
  __taintsan_propagate(res, src, n + 1);
  return res;
}

INTERCEPTOR(char *, strndup, char *src, SIZE_T n) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T copy_size = REAL(strnlen)(src, n);
  char *res = REAL(strndup)(src, n);
  __taintsan_propagate(res, src, copy_size);
  return res;
}

INTERCEPTOR(char *, __strndup, char *src, SIZE_T n) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T copy_size = REAL(strnlen)(src, n);
  char *res = REAL(__strndup)(src, n);
  __taintsan_propagate(res, src, copy_size);
  return res;
}

INTERCEPTOR(char *, strcat, char *dest, const char *src) { 
  ENSURE_TAINTSAN_INITED();
  SIZE_T src_size = REAL(strlen)(src);
  SIZE_T dest_size = REAL(strlen)(dest);
  char *res = REAL(strcat)(dest, src);  
  __taintsan_propagate(dest + dest_size, src, src_size + 1);
  return res;
}

INTERCEPTOR(char *, strncat, char *dest, const char *src, SIZE_T n) {  
  ENSURE_TAINTSAN_INITED();
  SIZE_T dest_size = REAL(strlen)(dest);
  SIZE_T copy_size = REAL(strlen)(src);
  if (copy_size < n)
    copy_size++;  // trailing \0
  char *res = REAL(strncat)(dest, src, n); 
  __taintsan_propagate(dest + dest_size, src, copy_size);
  return res;
}

INTERCEPTOR(int, vsnprintf, char *str, uptr size,
            const char *format, va_list ap) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T format_size = REAL(strlen)(format);
  CHECK_UNTAINTED(format, format_size, "vsnprintf");
  int res = REAL(vsnprintf)(str, size, format, ap);
  return res;
}

INTERCEPTOR(int, vsprintf, char *str, const char *format, va_list ap) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T format_size = REAL(strlen)(format);
  CHECK_UNTAINTED(format, format_size, "vsprintf");
  int res = REAL(vsprintf)(str, format, ap);
  return res;
}

INTERCEPTOR(int, printf, const char *format, ...) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T format_size = REAL(strlen)(format);
  CHECK_UNTAINTED(format, format_size, "printf");
  va_list ap;
  va_start(ap, format);
  int res = printf(format, ap);  
  va_end(ap);
  return res;
}

INTERCEPTOR(int, sprintf, char *str, const char *format, ...) { 
  ENSURE_TAINTSAN_INITED();
  SIZE_T format_size = REAL(strlen)(format);
  CHECK_UNTAINTED(format, format_size, "sprintf");
  va_list ap;
  va_start(ap, format);
  int res = vsprintf(str, format, ap);  
  va_end(ap);
  return res;
}

INTERCEPTOR(int, snprintf, char *str, uptr size, const char *format, ...) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T format_size = REAL(strlen)(format);
  CHECK_UNTAINTED(format, format_size, "snprintf");
  va_list ap;
  va_start(ap, format);
  int res = vsnprintf(str, size, format, ap);
  va_end(ap);
  return res;
}

// SIZE_T strftime(char *s, SIZE_T max, const char *format,const struct tm *tm);
INTERCEPTOR(SIZE_T, strftime, char *s, SIZE_T max, const char *format,
            void *tm) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T format_size = REAL(strlen)(format);
  CHECK_UNTAINTED(format, format_size, "strftime");
  SIZE_T res = REAL(strftime)(s, max, format, tm);
  return res;
}

INTERCEPTOR(SIZE_T, wcslen, const wchar_t *s) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T res = REAL(wcslen)(s);
  CHECK_UNTAINTED(s, sizeof(wchar_t) * (res + 1), "wcslen");
  return res;
}

// wchar_t *wcscpy(wchar_t *dest, const wchar_t *src);
INTERCEPTOR(wchar_t *, wcscpy, wchar_t *dest, const wchar_t *src) {
  ENSURE_TAINTSAN_INITED();
  wchar_t *res = REAL(wcscpy)(dest, src);
  __taintsan_propagate(dest, src, sizeof(wchar_t) * (REAL(wcslen)(src) + 1));
  return res;
}

// wchar_t *wmemcpy(wchar_t *dest, const wchar_t *src, SIZE_T n);
INTERCEPTOR(wchar_t *, wmemcpy, wchar_t *dest, const wchar_t *src, SIZE_T n) {
  ENSURE_TAINTSAN_INITED();
  wchar_t *res = REAL(wmemcpy)(dest, src, n);
  __taintsan_propagate(dest, src, n * sizeof(wchar_t));
  return res;
}

INTERCEPTOR(wchar_t *, wmemmove, wchar_t *dest, const wchar_t *src, SIZE_T n) {
  ENSURE_TAINTSAN_INITED();
  wchar_t *res = REAL(wmemmove)(dest, src, n);
  __taintsan_move_taint(dest, src, n * sizeof(wchar_t));
  return res;
}

INTERCEPTOR(int, wcscmp, const wchar_t *s1, const wchar_t *s2) {
  ENSURE_TAINTSAN_INITED();
  int res = REAL(wcscmp)(s1, s2);
  return res;
}

INTERCEPTOR(char *, getenv, char *name) {
  ENSURE_TAINTSAN_INITED();
  char *res = REAL(getenv)(name);
  if (res)
    __taintsan_add_taint(res, REAL(strlen)(res) + 1);
  return res;
}

INTERCEPTOR(char *, fgets, char *s, int size, void *stream) {
  ENSURE_TAINTSAN_INITED();
  char *res = REAL(fgets)(s, size, stream);
  if (res)
    __taintsan_add_taint(s, REAL(strlen)(s) + 1);
  return res;
}

INTERCEPTOR(char *, fgets_unlocked, char *s, int size, void *stream) {
  ENSURE_TAINTSAN_INITED();
  char *res = REAL(fgets_unlocked)(s, size, stream);
  if (res)
    __taintsan_add_taint(s, REAL(strlen)(s) + 1);
  return res;
}

INTERCEPTOR(char *, getcwd, char *buf, SIZE_T size) {
  ENSURE_TAINTSAN_INITED();
  char *res = REAL(getcwd)(buf, size);
  if (res)
    __taintsan_add_taint(res, REAL(strlen)(res) + 1);
  return res;
}

INTERCEPTOR(char *, realpath, char *path, char *abspath) {
  ENSURE_TAINTSAN_INITED();
  char *res = REAL(realpath)(path, abspath);
  if (res)
    __taintsan_add_taint(abspath, REAL(strlen)(abspath) + 1);
  return res;
}

INTERCEPTOR(int, gethostname, char *name, SIZE_T len) {
  ENSURE_TAINTSAN_INITED();
  int res = REAL(gethostname)(name, len);
  if (!res) {
    SIZE_T real_len = REAL(strnlen)(name, len);
    if (real_len < len)
      ++real_len;
    __taintsan_add_taint(name, real_len);
  }
  return res;
}

INTERCEPTOR(SSIZE_T, recv, int fd, void *buf, SIZE_T len, int flags) {
  ENSURE_TAINTSAN_INITED();
  SSIZE_T res = REAL(recv)(fd, buf, len, flags);
  if (res > 0)
    __taintsan_add_taint(buf, res);
  return res;
}

INTERCEPTOR(SSIZE_T, recvfrom, int fd, void *buf, SIZE_T len, int flags,
    void *srcaddr, void *addrlen) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T srcaddr_sz;
  if (srcaddr)
    srcaddr_sz = __sanitizer_get_socklen_t(addrlen);
  SSIZE_T res = REAL(recvfrom)(fd, buf, len, flags, srcaddr, addrlen);
  if (res > 0) {
    __taintsan_add_taint(buf, res);
    if (srcaddr) {
      SIZE_T sz = __sanitizer_get_socklen_t(addrlen);
      __taintsan_add_taint(srcaddr, (sz < srcaddr_sz) ? sz : srcaddr_sz);
    }
  }
  return res;
}

INTERCEPTOR(SSIZE_T, recvmsg, int fd, struct msghdr *msg, int flags) {
  ENSURE_TAINTSAN_INITED();
  SSIZE_T res = REAL(recvmsg)(fd, msg, flags);
  if (res > 0) {
    for (SIZE_T i = 0; i < __sanitizer_get_msghdr_iovlen(msg); ++i)
      __taintsan_add_taint(__sanitizer_get_msghdr_iov_iov_base(msg, i),
          __sanitizer_get_msghdr_iov_iov_len(msg, i));
  }
  return res;
}

INTERCEPTOR(void *, calloc, SIZE_T nmemb, SIZE_T size) {
  if (CallocShouldReturnNullDueToOverflow(size, nmemb)) return 0;
  StackTrace stack; 
  stack.size = 0;
  if (!taintsan_inited) {
    // Hack: dlsym calls calloc before REAL(calloc) is retrieved from dlsym.
    const SIZE_T kCallocPoolSize = 1024;
    static uptr calloc_memory_for_dlsym[kCallocPoolSize];
    static SIZE_T allocated;
    SIZE_T size_in_words = ((nmemb * size) + kWordSize - 1) / kWordSize;
    void *mem = (void*)&calloc_memory_for_dlsym[allocated];
    allocated += size_in_words;
    CHECK(allocated < kCallocPoolSize);
    return mem;
  }
  return TaintsanReallocate(&stack, 0, nmemb * size, sizeof(u64), true);
}

INTERCEPTOR(void *, realloc, void *ptr, SIZE_T size) {
  StackTrace stack; 
  stack.size = 0;
  return TaintsanReallocate(&stack, ptr, size, sizeof(u64), false);
}

INTERCEPTOR(void *, malloc, SIZE_T size) {
  StackTrace stack; 
  stack.size = 0;
  return TaintsanReallocate(&stack, 0, size, sizeof(u64), false);
}

INTERCEPTOR(void *, dlopen, const char *filename, int flag) {
  ENSURE_TAINTSAN_INITED();
  SIZE_T size = REAL(strlen)(filename);
  CHECK_UNTAINTED(filename, size, "dlopen");
  link_map *map = (link_map *)REAL(dlopen)(filename, flag);
  if (map) {
    UnpoisonMappedDSO(map);
  }
  return (void *)map;
}

extern "C" int pthread_attr_init(void *attr);
extern "C" int pthread_attr_destroy(void *attr);
extern "C" int pthread_attr_setstacksize(void *attr, uptr stacksize);
extern "C" int pthread_attr_getstack(void *attr, uptr *stack, uptr *stacksize);

INTERCEPTOR(int, pthread_create, void *th, void *attr, void *(*callback)(void*),
            void * param) {
  ENSURE_TAINTSAN_INITED(); // for GetTlsSize()
  __sanitizer_pthread_attr_t myattr;
  if (attr == 0) {
    pthread_attr_init(&myattr);
    attr = &myattr;
  }

  AdjustStackSizeLinux(attr, flags()->verbosity);

  int res = REAL(pthread_create)(th, attr, callback, param);
  if (attr == &myattr)
    pthread_attr_destroy(&myattr);
  return res;
}

#define COMMON_INTERCEPTOR_WRITE_RANGE(ctx, ptr, size) \
    __taintsan_untaint(ptr, size)
#define COMMON_INTERCEPTOR_READ_RANGE(ctx, ptr, size) do { } while (false)
#define COMMON_INTERCEPTOR_ENTER(ctx, func, ...)  \
  do {                                            \
    if (taintsan_init_is_running)                     \
      return REAL(func)(__VA_ARGS__);             \
    ctx = 0;                                      \
    (void)ctx;                                    \
    ENSURE_TAINTSAN_INITED();                         \
  } while (false)
#define COMMON_INTERCEPTOR_FD_ACQUIRE(ctx, fd) do { } while (false)
#define COMMON_INTERCEPTOR_FD_RELEASE(ctx, fd) do { } while (false)
#define COMMON_INTERCEPTOR_SET_THREAD_NAME(ctx, name) \
  do { } while (false)  // FIXME
#include "sanitizer_common/sanitizer_common_interceptors.inc"

#define COMMON_SYSCALL_PRE_READ_RANGE(p, s) CHECK_UNTAINTED(p, s, "syscall")
#define COMMON_SYSCALL_PRE_WRITE_RANGE(p, s)
#define COMMON_SYSCALL_POST_READ_RANGE(p, s)
#define COMMON_SYSCALL_POST_WRITE_RANGE(p, s) __taintsan_untaint(p, s)
#include "sanitizer_common/sanitizer_common_syscalls.inc"

// static
void *fast_memset(void *ptr, int c, SIZE_T n) {
  // hack until we have a really fast internal_memset
  if (sizeof(uptr) == 8 &&
      (n % 8) == 0 &&
      ((uptr)ptr % 8) == 0 &&
      (c == 0 || c == -1)) {
    // Printf("memset %p %zd %x\n", ptr, n, c);
    uptr to_store = c ? -1L : 0L;
    uptr *p = (uptr*)ptr;
    for (SIZE_T i = 0; i < n / 8; i++)
      p[i] = to_store;
    return ptr;
  }
  return internal_memset(ptr, c, n);
}

// static
void *fast_memcpy(void *dst, const void *src, SIZE_T n) {
  // Same hack as in fast_memset above.
  if (sizeof(uptr) == 8 &&
      (n % 8) == 0 &&
      ((uptr)dst % 8) == 0 &&
      ((uptr)src % 8) == 0) {
    uptr *d = (uptr*)dst;
    uptr *s = (uptr*)src;
    for (SIZE_T i = 0; i < n / 8; i++)
      d[i] = s[i];
    return dst;
  }
  return internal_memcpy(dst, src, n);
}

// These interface functions reside here so that they can use
// fast_memset, etc.
void __taintsan_untaint(const void *a, uptr size) {
  if (!MEM_IS_APP(a)) return;
  fast_memset((void*)MEM_TO_SHADOW((uptr)a), 0, size);
}

void __taintsan_add_taint(const void *a, uptr size) {
  if (!MEM_IS_APP(a)) return;
  fast_memset((void*)MEM_TO_SHADOW((uptr)a), -1, size);
}

void __taintsan_add_taint_stack(void *a, uptr size) {
  if (!MEM_IS_APP(a)) return;
  fast_memset((void*)MEM_TO_SHADOW((uptr)a), -1, size);
}

void __taintsan_clear_and_unpoison(void *a, uptr size) {
  fast_memset(a, 0, size);
  fast_memset((void*)MEM_TO_SHADOW((uptr)a), 0, size);
}

void __taintsan_propagate(void *dst, const void *src, uptr size) {
  if (!MEM_IS_APP(dst)) return;
  if (!MEM_IS_APP(src)) return;
  fast_memcpy((void*)MEM_TO_SHADOW((uptr)dst),
              (void*)MEM_TO_SHADOW((uptr)src), size);
}

void __taintsan_move_taint(void *dst, const void *src, uptr size) {
  if (!MEM_IS_APP(dst)) return;
  if (!MEM_IS_APP(src)) return;
  internal_memmove((void*)MEM_TO_SHADOW((uptr)dst),
         (void*)MEM_TO_SHADOW((uptr)src), size);
}

void *__taintsan_memcpy(void *dest, const void *src, SIZE_T n) {
  ENSURE_TAINTSAN_INITED();
  void *res = fast_memcpy(dest, src, n);
  __taintsan_propagate(dest, src, n);
  return res;
}

void *__taintsan_memset(void *s, int c, SIZE_T n) {
  ENSURE_TAINTSAN_INITED();
  void *res = fast_memset(s, c, n);
  __taintsan_untaint(s, n);
  return res;
}

void *__taintsan_memmove(void *dest, const void *src, SIZE_T n) {
  ENSURE_TAINTSAN_INITED();
  void *res = REAL(memmove)(dest, src, n);
  __taintsan_move_taint(dest, src, n);
  return res;
}

namespace __taintsan {
void InitializeInterceptors() {
  static int inited = 0;
  CHECK_EQ(inited, 0);
  SANITIZER_COMMON_INTERCEPTORS_INIT;

  INTERCEPT_FUNCTION(malloc);
  INTERCEPT_FUNCTION(calloc);
  INTERCEPT_FUNCTION(realloc);
  INTERCEPT_FUNCTION(free);
  INTERCEPT_FUNCTION(fread);
  INTERCEPT_FUNCTION(fread_unlocked);
  INTERCEPT_FUNCTION(memcpy);
  INTERCEPT_FUNCTION(memset);
  INTERCEPT_FUNCTION(memmove);
  INTERCEPT_FUNCTION(wmemcpy);
  INTERCEPT_FUNCTION(wmemmove);
  INTERCEPT_FUNCTION(strcpy);
  INTERCEPT_FUNCTION(strdup);
  INTERCEPT_FUNCTION(__strdup);
  INTERCEPT_FUNCTION(strndup);
  INTERCEPT_FUNCTION(__strndup);
  INTERCEPT_FUNCTION(strncpy);
  INTERCEPT_FUNCTION(strlen);
  INTERCEPT_FUNCTION(strnlen);
  INTERCEPT_FUNCTION(strcat); 
  INTERCEPT_FUNCTION(strncat);
  INTERCEPT_FUNCTION(vsprintf);
  INTERCEPT_FUNCTION(vsnprintf);
  INTERCEPT_FUNCTION(sprintf); 
  INTERCEPT_FUNCTION(printf); 
  INTERCEPT_FUNCTION(snprintf);
  INTERCEPT_FUNCTION(strftime);
  INTERCEPT_FUNCTION(wcslen);
  INTERCEPT_FUNCTION(wcscpy);
  INTERCEPT_FUNCTION(wcscmp);
  INTERCEPT_FUNCTION(getenv);
  INTERCEPT_FUNCTION(fgets);
  INTERCEPT_FUNCTION(fgets_unlocked);
  INTERCEPT_FUNCTION(getcwd);
  INTERCEPT_FUNCTION(realpath);
  INTERCEPT_FUNCTION(gethostname);
  INTERCEPT_FUNCTION(recv);
  INTERCEPT_FUNCTION(recvfrom);
  INTERCEPT_FUNCTION(recvmsg);
  INTERCEPT_FUNCTION(dlopen);
  INTERCEPT_FUNCTION(pthread_create);
  inited = 1;
}
}  // namespace __taintsan
