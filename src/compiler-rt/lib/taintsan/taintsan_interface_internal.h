//===-- taintsan_interface_internal.h ---------------------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file is a part of TaintSanitizer.
//
// Private TainSan interface header.
//===----------------------------------------------------------------------===//

#ifndef TAINTSAN_INTERFACE_INTERNAL_H
#define TAINTSAN_INTERFACE_INTERNAL_H

#include "sanitizer_common/sanitizer_internal_defs.h"
extern "C" {
SANITIZER_INTERFACE_ATTRIBUTE
void __taintsan_init();

SANITIZER_INTERFACE_ATTRIBUTE
void __taintsan_warning(const char * info);

SANITIZER_INTERFACE_ATTRIBUTE __attribute__((noreturn))
void __taintsan_warning_noreturn(const char * info);

SANITIZER_INTERFACE_ATTRIBUTE
void __taintsan_add_taint_stack(void *a, uptr size);
SANITIZER_INTERFACE_ATTRIBUTE
void __taintsan_add_taint(const void *a, uptr size);
SANITIZER_INTERFACE_ATTRIBUTE
void __taintsan_propagate(void *dst, const void *src, uptr size);
SANITIZER_INTERFACE_ATTRIBUTE
void __taintsan_move_taint(void *dst, const void *src, uptr size);
SANITIZER_INTERFACE_ATTRIBUTE
void __taintsan_untaint(const void *a, uptr size);

SANITIZER_INTERFACE_ATTRIBUTE
void* __taintsan_memcpy(void *dst, const void *src, uptr size);
SANITIZER_INTERFACE_ATTRIBUTE
void* __taintsan_memset(void *s, int c, uptr n);
SANITIZER_INTERFACE_ATTRIBUTE
void* __taintsan_memmove(void* dest, const void* src, uptr n);

// Copy size bytes from src to dst and untaint the result.
SANITIZER_INTERFACE_ATTRIBUTE
void __taintsan_load_untainted(void *src, uptr size, void *dst);

// Returns the offset of the first (at least partially) tainted byte,
// or -1 if the whole range is good.
SANITIZER_INTERFACE_ATTRIBUTE
sptr __taintsan_test_shadow(const void *x, uptr size);

SANITIZER_INTERFACE_ATTRIBUTE
void __taintsan_clear_on_return();

// Default: -1 (don't exit on error).
SANITIZER_INTERFACE_ATTRIBUTE
void __taintsan_set_exit_code(int exit_code);

SANITIZER_WEAK_ATTRIBUTE SANITIZER_INTERFACE_ATTRIBUTE
/* OPTIONAL */ const char* __taintsan_default_options();


SANITIZER_INTERFACE_ATTRIBUTE
void __taintsan_partial_taint(const void* data, void* shadow, uptr size);

// Returns x such that %fs:x is the first byte of __taintsan_retval_tls.
SANITIZER_INTERFACE_ATTRIBUTE
int __taintsan_get_retval_tls_offset();
SANITIZER_INTERFACE_ATTRIBUTE
int __taintsan_get_param_tls_offset();
}  // extern "C"

#endif  // TAINTSAN_INTERFACE_INTERNAL_H